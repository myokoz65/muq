
#ifndef _Auctioneer_h
#define _Auctioneer_h

// eigen includes
#include <Eigen/Core>

// std library includes
#include <vector>
#include <list>
#include <limits>
#include <type_traits>

// boost includes
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace muq {
namespace Optimization {
template<typename StoreScalar = float>
class Bidder {
public:

  template<typename derived1, typename derived2>
  Bidder(const Eigen::DenseBase<derived1>& dists, const Eigen::DenseBase<derived2>& prices) : benefit(dists), price(
                                                                                                prices),
                                                                                              needUpdate(true)
  {}

  template<typename InputScalar>
  Bidder(const Eigen::Matrix<InputScalar, Eigen::Dynamic,
                             1>& BidderLoc,
         const Eigen::Matrix<InputScalar, Eigen::Dynamic,
                             Eigen::Dynamic>& ObjLocs,
         const Eigen::Matrix<InputScalar, Eigen::Dynamic, 1>& prices) : benefit(ObjLocs.cols()), price(
                                                                          prices.template cast<StoreScalar>()),
                                                                        needUpdate(true)
  {
    // make sure the vectors are the correct sizes
    assert(BidderLoc.rows() == ObjLocs.rows());
    assert(BidderLoc.cols() == 1);
    assert(ObjLocs.cols() == prices.rows());
    assert(prices.cols() == 1);

    // fill in the benefit vector
    for (unsigned int i = 0; i < ObjLocs.cols(); ++i) {
      benefit(i) = StoreScalar(-1.0 * (BidderLoc - ObjLocs.col(i)).squaredNorm());
    }
  }

  // update the price to reflect current auction information
  template<typename InputScalar>
  void UpdatePrice(unsigned int Object, InputScalar NewPrice)
  {
    price(Object) = StoreScalar(NewPrice);

    // we will only need to update the prices if we set one of the best objects
    if (Object == secObject) {
      needUpdate = true;
    } else if (Object == bestObject) {
      if ((benefit(bestObject) - NewPrice) <= (benefit(secObject) - price(secObject))) {
        needUpdate = true;
      }
    }
  }

  // get a bid
  template<typename InputScalar>
  void GetBid(InputScalar eps, unsigned int& Object, InputScalar& Bid)
  {
    if (needUpdate) {
      UpdateFavorite();
    }

    Object = bestObject;
    Bid    = InputScalar(price[bestObject] + maxVal - secVal) + eps; // increment with the second largest value
  }

private:

  StoreScalar  maxVal, secVal;
  unsigned int bestObject, secObject;

  Eigen::Matrix<StoreScalar, Eigen::Dynamic, 1> benefit;
  Eigen::Matrix<StoreScalar, Eigen::Dynamic, 1> price;

  bool needUpdate;


  void UpdateFavorite()
  {
    // find the maximum and second largest values
    maxVal = benefit(0) - price(0);
    secVal = benefit(1) - price(1);
    if (maxVal > secVal) {
      bestObject = 0;
      secObject  = 1;
    } else {
      std::swap<StoreScalar>(maxVal, secVal);

      bestObject = 1;
      secObject  = 0;
    }

    for (unsigned int j = 2; j < benefit.size(); ++j) {
      StoreScalar tempVal = benefit(j) - price(j);

      if (tempVal > maxVal) {
        secVal = maxVal;
        maxVal = tempVal;

        secObject  = bestObject;
        bestObject = j;
      } else if (tempVal > secVal) {
        secVal    = tempVal;
        secObject = j;
      }
    }
    needUpdate = false;
  }
};


/** @class Auctioneer
 *  @author Matthew Parno
 *  @ingroup Optimization
 *  @brief Implementation of the Auction algorithm for solving the assignment problem
 *  @details This class solves the symmetric assignment problem.  Given \f$ N \f$ people (those bidding in the auction),
 *     \f$ N \f$ objects (what the people are bidding for), and the value \f$ a_{ij} \f$ of object \f$ j \f$ to person
 *     \f$ i \f$, this algorithm maximizes the total value over all people by assigning people to objects.  The basic
 *     problem implemented here is to match points in an input space to points in an output space by minimizing the
 * total
 *     distance between the pairs.
 */
template<typename BaseType = float>
class Auctioneer {
public:

  template<typename InputScalar>
  Auctioneer(const Eigen::Matrix<InputScalar, Eigen::Dynamic, Eigen::Dynamic>& dists) : Npts(dists.cols()), forwardAss(
                                                                                          dists.cols()), backwardAss(
                                                                                          dists.cols())
  {
    Eigen::Matrix<BaseType, Eigen::Dynamic, 1> Prices = Eigen::Matrix<BaseType, Eigen::Dynamic, 1>::Zero(dists.cols());

    //std::cout << "herea\n";
    // initialize all the bidders
    Eigen::Matrix<BaseType, Eigen::Dynamic, 1> costs(dists.cols());

    for (unsigned int i = 0; i < Npts; ++i) {
      costs = dists.row(i).transpose().template cast<BaseType>();
      Bidders.push_back(Bidder<BaseType>(costs, Prices));
    }
  }

  template<typename InputScalar>
  Auctioneer(const Eigen::Matrix<InputScalar, Eigen::Dynamic,
                                 Eigen::Dynamic>& xi,
             const Eigen::Matrix<InputScalar, Eigen::Dynamic,
                                 Eigen::Dynamic>& xo) : Npts(xi.cols()), forwardAss(xi.cols()), backwardAss(xi.cols())
  {
    Eigen::Matrix<BaseType, Eigen::Dynamic, 1> Prices = Eigen::Matrix<BaseType, Eigen::Dynamic, 1>::Zero(Npts);

    // initialize all the bidders
    for (unsigned int i = 0; i < Npts; ++i) {
      // build the vector of distances
      Eigen::Matrix<BaseType, Eigen::Dynamic, 1> costs(Npts);
      for (unsigned int k = 0; k < Npts; ++k) {
        costs(k) = BaseType(-1.0 * (xi.col(i) - xo.col(k)).squaredNorm());
      }

      Bidders.push_back(Bidder<BaseType>(costs, Prices));
    }
  }

  void solve()
  {
    boost::property_tree::ptree emptyTree;

    solve(emptyTree);
  }

  void solve(const std::string& paramFile)
  {
    boost::property_tree::ptree temp;
    boost::property_tree::read_xml(paramFile, temp);

    solve(temp);
  }

  /** Solve the assignement problem defined in the constructor using the given initial prices.
   *  @param[in] p0 Initial prices
   */
  void solve(boost::property_tree::ptree& params)
  {
    // store the assignments from ref to posterior, that is ref(ind)=post
    for (unsigned int i = 0; i < Npts; ++i) {
      forwardAss[i]  = i;
      backwardAss[i] = i;
    }

    BaseType tol   = params.get("Opt.Auction.EpsTol", 0.002);
    BaseType eps   = params.get("Opt.Auction.MaxEps", 0.5);
    BaseType scale = params.get("Opt.Auction.EpsScale", 0.125);
    int verbose    = params.get("Verbose", 0);

    if (verbose > 0) {
      std::cout << "Solving assignment problem using the Auction algorithm...\n";
    }

    // keep making the bidders "approximately happy" until they are completely happy
    while (eps > tol) {
      if (verbose > 0) {
        std::cout << "\tCurrently working on epsilon = " << eps << " problem.\n";
      }

      solveEps(eps);
      eps *= scale;
    }
  }

  /** Get the assignments found during the solve function.
   *  @return Assignement vector v.   v[i] holds the object assigned to person i.
   */
  std::vector<unsigned int> GetSolution() const
  {
    return forwardAss;
  }

private:

  /** Solve the problem until all bidders are "approximately happy." i.e. until we are within eps of being optimal.
   *   This routine is called within the solve function with a decaying epsilon.
   *  @param[in] p0 Initial prices of all the objects
   *  @param[in] eps The complimentary slackness level
   */
  void solveEps(BaseType eps)
  {
    // hold the cost
    Eigen::Matrix<BaseType, Eigen::Dynamic, 1> val = Eigen::Matrix<BaseType, Eigen::Dynamic, 1>::Zero(Npts);

    // compute the initial prices -- the negative distances
    std::vector<bool> happy(Npts, false);
    unsigned int Nhappy = 0;

    unsigned short int its = 0;

    while (Nhappy < Npts) {
      ++its;

      for (unsigned int i = 0; i < Npts; ++i) {
        if (!happy[i]) {
          // get the bid increment
          unsigned int maxInd;
          BaseType     newBid;
          Bidders[i].GetBid(eps, maxInd, newBid);

          // ////////////////////////////////////
          // update the assignment

          // find the person with the current index
          unsigned int ftemp = forwardAss[i];
          unsigned int btemp = backwardAss[maxInd];

          if (btemp == i) {
            Nhappy++;
            happy[i] = true;
          } else {
            if (!happy[btemp]) {
              Nhappy++;
            }

            happy[i]     = true;
            happy[btemp] = false;

            //bids[i] = max_val;
            forwardAss[btemp]   = ftemp;
            forwardAss[i]       = maxInd;
            backwardAss[ftemp]  = btemp;
            backwardAss[maxInd] = i;
          }

          for (unsigned int i = 0; i < Npts; ++i) {
            Bidders[i].UpdatePrice(maxInd, newBid);
          }
        }
      }
    }
  }

  unsigned int Npts;
  std::vector < Bidder < BaseType >> Bidders;

  std::vector<unsigned int> forwardAss;
  std::vector<unsigned int> backwardAss;
};
} // namespace Optimization
} // namespace muq

#endif // ifndef _Auctioneer_h

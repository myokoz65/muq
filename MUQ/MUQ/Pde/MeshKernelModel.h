
#ifndef MESHKERNELMODEL_H_
#define MESHKERNELMODEL_H_

#include <set>

#include "MUQ/Utilities/VectorTranslater.h"

#include "MUQ/Utilities/VariableNameManipulation.h"
#include "MUQ/Utilities/LibMeshTranslater.h"

#include "MUQ/Pde/KernelModel.h"
#include "MUQ/Pde/GenericEquationSystems.h"

#include "libmesh/numeric_vector.h"

namespace muq {
namespace Pde {
/// Kernel model for a parameter on a mesh
class MeshKernelModel : public KernelModel {
public:

  /// Create a kernel model with zero mean
  MeshKernelModel(std::string const                              & paraName,
                  std::shared_ptr<libMesh::EquationSystems> const& parasystem,
                  std::vector<Eigen::VectorXd> const             & pts,
                  unsigned int const                               numBasis,
                  std::shared_ptr<muq::Geostatistics::CovKernel> const& kernel);

  /// Create a kernel model with nonzero mean
  MeshKernelModel(std::string const                              & paraName,
                  std::shared_ptr<libMesh::EquationSystems> const& parasystem,
                  std::vector<Eigen::VectorXd> const             & pts,
                  unsigned int const                               numBasis,
                  Eigen::VectorXd const                          & mean,
                  std::shared_ptr<muq::Geostatistics::CovKernel> const& kernel);

  /// Construct a mesh kernel model with zero mean
  /**
   *  Options in the ptree:
   *  -# The name of the paramter (<EM>MeshKernelModel.Name</EM>)
   *  -# The order of the element basis functions (<EM>MeshKernelModel.Order</EM>)
   *       - 0 Constant on each element
   *       - 1 Linear on each element
   *       - 2 Quadratic on each element
   *  -# The number of KL modes functions (<EM>MeshKernelModel.KLModes</EM>)
   *       - Defaults to 10
   *  @param[in] parasystem The equation system holding the mesh on which the parameter is defined
   *  @param[in] kernel The covariance kernel
   *  @param[in] param Parameters for the kernel
   */
  static std::shared_ptr<MeshKernelModel> Construct(
    std::shared_ptr<muq::Pde::GenericEquationSystems> const& parasystem,
    std::shared_ptr<muq::Geostatistics::CovKernel> const        & kernel,
    boost::property_tree::ptree const                      & param);

  /// Construct a mesh kernel model with nonzero mean
  /**
   *  Options in the ptree:
   *  -# The name of the paramter (<EM>MeshKernelModel.Name</EM>)
   *  -# The order of the element basis functions (<EM>MeshKernelModel.Order</EM>)
   *       - 0 Constant on each element
   *       - 1 Linear on each element (Default)
   *       - 2 Quadratic on each element
   *  -# The number of KL modes functions (<EM>MeshKernelModel.KLModes</EM>)
   *       - Defaults to 10
   *  @param[in] parasystem The equation system holding the mesh on which the parameter is defined
   *  @param[in] kernel The covariance kernel
   *  @param[in] mean The mean
   *  @param[in] param Parameters for the kernel
   */
  static std::shared_ptr<MeshKernelModel> Construct(
    std::shared_ptr<muq::Pde::GenericEquationSystems> const& parasystem,
    std::shared_ptr<muq::Geostatistics::CovKernel> const        & kernel,
    Eigen::VectorXd const                                  & mean,
    boost::property_tree::ptree const                      & param);

private:

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& inputs) override;

  const std::string paraName;

  std::shared_ptr<libMesh::EquationSystems> parasystem;
};
}
}

#endif // ifndef MESHKERNELMODEL_H_


#ifndef POINTSENSOR_H_
#define POINTSENSOR_H_

#include "libmesh/numeric_vector.h"

#include "MUQ/Utilities/VectorTranslater.h"

#include "MUQ/Modelling/ModPiece.h"

#include "MUQ/Pde/GenericEquationSystems.h"

namespace muq {
namespace Pde {
/// Sensor to compute a field at specific points
class PointSensor : public muq::Modelling::ModPiece {
public:

  /// Create a point sensor given an equation system, the system name, and points
  /**
   *  @param[in] inputSizes The number of points in the field.  It must be the same as the size of the solution vector
   *in the equation system.
   *  @param[in] points A vector of points.  Each Eigen::VectorXd must be the same size as the number of dimensions
   *  @param[in] sysName The name of the system (it must already be in eqnsystem)
   *  @param[in] eqnsystem The equation system that contains the specific system being sensed
   */
  PointSensor(int const                                     inputSize,
              std::vector<Eigen::VectorXd> const          & points,
              std::string const                           & sysName,
              std::shared_ptr<GenericEquationSystems> const eqnsystem);

private:

  /// Compute the field at specified points
  /**
   *  Projects the input onto PointSensor::sysName and then computes the solution at PointSensor::points. It returns the
   *solution at each sensor in the same order as the points are listed.  If there is more than one unknown in the system
   *the order is
   *  \f{equation}{
   *  \left[\begin{array}{ccccccccccc}
   *  v_1(\mathbf{x}_1) & v_2(\mathbf{x}_1) & ... & v_n(\mathbf{x}_1) & | & ... & | & v_1(\mathbf{x}_m) &
   * v_2(\mathbf{x}_m) & ... & v_n(\mathbf{x}_m)
   *  \end{array} \right]^T.
   *  \f}
   */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt) override;

  /// A list of points where there are sensors
  std::vector<libMesh::Point> points;

  /// A list of the element corresponding to each point
  std::vector<const libMesh::Elem*> elements;

  /// The equation system where the field is stored
  std::shared_ptr<libMesh::EquationSystems> system;
};
} // namespace Pde
} // namespace muq

#endif // ifndef POINTSENSOR_H_

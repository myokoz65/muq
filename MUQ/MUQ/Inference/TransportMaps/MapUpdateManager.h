
#ifndef MapUpdateManager_h_
#define MapUpdateManager_h_

#include "MUQ/Inference/TransportMaps/TransportMap.h"

// boost includes
#include <boost/property_tree/ptree.hpp>

namespace muq {
namespace Inference {
/** @class MapUpdateManager
 *  @brief Provides a mechanism for update a transport map as more samples are included for training.
 *  @ingroup Inference
 */
class MapUpdateManager {
public:

  MapUpdateManager(std::shared_ptr<TransportMap> mapIn, int expectedSamps, double priorPrecisionIn = 0.0);

  // update the map to reflect any new samples.
  void  UpdateMap(const Eigen::Ref<const Eigen::MatrixXd>& newSamps,
                  std::vector<Eigen::VectorXi>             optLevels = std::vector<Eigen::VectorXi>(),
	                boost::property_tree::ptree              options   = boost::property_tree::ptree());

  // return a shared_ptr to the map
  std::shared_ptr<TransportMap> GetMap() const{return map;}

  Eigen::VectorXd GetInducedDensities(int startInd, int segmentLength) const;
  
  /** @brief Return the error between the current marginal distributions and a standard Normal distribution.
   *  @details This function compares quantiles of the map output (using the current samples) to the quantiles of a standard normal distribution.  The average relative error for each dimension is returned.
   * @param[in] numBins How many quantiles should we compare?
   * @param[in] qLB The smallest quantile to include in the error estimation
   * @param[in] qUB The largest quantile to include in the error estimation
   * @return A D dimensional vector holding the average quantile error in each dimension.
   */ 
  Eigen::VectorXd GetMarginalErrors(int numBins, double qLB, double qUB) const;

private:

  // add the new f and g evaluations to the F and G matrices
  void UpdateBasisEvaluations(const Eigen::Ref<const Eigen::MatrixXd>& newSamps);
  
  // add the new f and g evaluations to the F and G matrices
  void UpdateBasisEvaluations(int dim, const Eigen::Ref<const Eigen::MatrixXd>& newSamps);

  // do rank updates to the F^T * F hessian matrix
  void UpdateHessians(int dim);

  // compute the map coefficients for a single dimension
  Eigen::VectorXd MinimizeKL(int dim, const Eigen::VectorXi &optLevels, boost::property_tree::ptree &options);
  Eigen::VectorXd MinimizeKL(int d, const int maxTerm, const Eigen::VectorXd &x0, boost::property_tree::ptree &options);

  std::shared_ptr<TransportMap> map; // a shared_ptr to the transport map this class is managing

  int oldNumSamps, newNumSamps;      // the current number of samples used during map construction

  const int maxNumSamps;             // the maximum number of samples we expect

  const double priorPrecision;       // a regularization term for how close to the initial map we want to stay

  // LDLT decompositions of the F^TF matrix
  std::vector < std::shared_ptr < Eigen::MatrixXd >> ffs;

  // store the values and gradients of the basis functions at each training point
  std::vector < std::shared_ptr < Eigen::MatrixXd >> fs, gs;

  // store the coefficients to the initial map for future regularization
  std::vector<Eigen::VectorXd> initialCoeffs;
};
}
}


#endif // ifndef MapUpdateManager_h_
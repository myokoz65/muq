#ifndef STATICPOLYNOMIALSPYTHON_H_
#define STATICPOLYNOMIALSPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Inference/TransportMaps/StaticPolynomials.h"

namespace muq {
namespace Inference {
void ExportStaticHermitePoly();
void ExportStaticLegendrePoly();
void ExportStaticMonomial();
}
}

#endif // ifndef STATICPOLYNOMIALSPYTHON_H_

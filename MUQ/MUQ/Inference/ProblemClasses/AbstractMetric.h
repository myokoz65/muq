
#ifndef ABSTRACTMETRIC_H
#define ABSTRACTMETRIC_H


#include <Eigen/Dense>


namespace muq {
namespace Inference {
/**
 * Abstract class for computing metrics. Subclasses must need only provide
 * an override of the ComputeMetric function.
 **/
class AbstractMetric {
public:

  AbstractMetric() = default;

  virtual ~AbstractMetric();

  /**
   * Core abstract method for this class, perform computation of the local metric.
   *
   **/
  virtual Eigen::MatrixXd ComputeMetric(Eigen::VectorXd const& point) = 0;

protected:
};
}
}

#endif // ABSTRACTMETRIC_H

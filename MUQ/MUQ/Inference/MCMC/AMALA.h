
#ifndef _AMALA_h
#define _AMALA_h

// include other MUQ related headers
#include "MUQ/Inference/MCMC/MCMCProposal.h"

#include "MUQ/Modelling/GaussianPair.h"

namespace muq {
namespace Inference {
	
	/** @class AMALA 
	    @ingroup Inference
	    @brief Implements a MALA proposal with adaptive covariance.
	    @details This class implements the adaptive MALA proposal from "An adaptive version for the Metropolis adjusted Langevin algorithm with a truncated drift." The proposal
	             is essentially the same as the standard MALA proposal (used in the PreMALA class), but the covariance is adapted from the samples (as in the AM class).
	*/
class AMALA : public MCMCProposal {
public:

  /** Default constructor. */
  AMALA(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
          boost::property_tree::ptree                            & properties);

  virtual ~AMALA() = default;

  virtual std::shared_ptr<muq::Inference::MCMCState> DrawProposal(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
    int const currIteration) override;

  virtual void PostProposalProcessing(
    std::shared_ptr<muq::Inference::MCMCState> const newState,
    MCMCProposal::ProposalStatus const               proposalAccepted,
    int const                                        iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry) override;

  virtual double ProposalDensity(
    std::shared_ptr<muq::Inference::MCMCState> currentState,
    std::shared_ptr<muq::Inference::MCMCState> proposedState) override;


  /** Write important information about the settings in this kernel as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName, std::string prefix = "") const;

private:

  void updateSampleMeanAndCov(int const currIteration, std::shared_ptr<muq::Inference::MCMCState> const currentState);
  void updateProp();

  double eps1       = 1e-3;  // lower bound on step size
  const double eps2       = 1e-10; // covariance nugget
  double A1         = 1e12;  // maximum frobenius norm of covariance
  double targetAcceptance; // target acceptance rate
  
  unsigned numProposals=0, numProposalsAccepted=0;
  double maxDrift; // this is the delta variable in the Atchade paper
  double stepSize, startVar;
  
  unsigned adaptSteps, adaptStart;
  
  // symmetric matrix to hold chain covariance
  Eigen::MatrixXd sampleCov;
  Eigen::LDLT<Eigen::MatrixXd> covSolver;
  
  // vector to hold chain mean
  Eigen::VectorXd sampleMean;

  std::shared_ptr<muq::Modelling::GaussianPair> proposal;
  
};
} //namespace muq
} // namespace Inference


#endif // ifndef _AMALA_h

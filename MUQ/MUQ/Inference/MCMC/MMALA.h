
#ifndef _MMALA_H_
#define _MMALA_H_


// include other MUQ related headers
#include "MUQ/Inference/MCMC/MCMCProposal.h"

#include "MUQ/Modelling/GaussianPair.h"

namespace muq {
namespace Inference {
class MMALA : public MCMCProposal {
public:

  /** Default constructor. */
  MMALA(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
        boost::property_tree::ptree                            & properties);


  /** destructor. */
  virtual ~MMALA() = default;

  virtual std::shared_ptr<muq::Inference::MCMCState> DrawProposal(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
    int const currIteration) override;

  virtual void PostProposalProcessing(
    std::shared_ptr<muq::Inference::MCMCState> const newState,
    MCMCProposal::ProposalStatus const               proposalAccepted,
    int const                                        iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry) override;

  virtual double ProposalDensity(
    std::shared_ptr<muq::Inference::MCMCState> currentState,
    std::shared_ptr<muq::Inference::MCMCState> proposedState) override;

  /** Write important information about the settings in this kernel as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName,
                               std::string        prefix = "") const;

protected:

  ///Construct a Gaussian density from which the proposed move can be drawn
  virtual std::shared_ptr<muq::Modelling::GaussianPair> ConstructMMALAProposal(std::shared_ptr<MCMCState> const state);

private:

  double stepLength;
  double acceptanceTarget;
  double stepLengthScaling;
  double stepLengthExp = 0;

  int adaptDuration;
  int acceptedThisBatch    = 0;
  int iterationWithinBatch = 0;
  int adaptBatchSize;
};
} //namespace muq
} // namespace muq


#endif // ifndef _MMALA_H_

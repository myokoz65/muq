/** @defgroup PolynomialChaos
 *  @ingroup Approximation
 *  @brief Tools for constructing and evaluating polynomial chaos expansions
 @details
    Polynomial chaos expansions are commonly used in uncertainty quantification for approximating computationally expensive simulations with relatively cheap multivariate polynomial expansions.  In MUQ, we provide capabilities for non-intrusive construction of polynomial chaos expansions based mostly on the pseudo-spectral approach of <a href="http://epubs.siam.org/doi/abs/10.1137/120890715">Conrad and Marzouk 2013</a>.
 
    <h2>Typical Usages</h2>
     In general, constructing a PCE in MUQ can be accomplished in three high-level steps:
     -# Defining the model we wish to approximate.  Models are described in MUQ by creating a child of the muq::Modelling::ModPiece class or piecing together several components with the muq::Modelling::Modgraph class.  See the Modelling module for more details.
     -# Defining the polynomials and quadrature rules in each dimension.  This is done by creating what we call a VariableCollection.
     -# Creating and running a PCE factory.  This is where all the work happens.  The factory will run the model at quadrature points, it might adapt the basis used to represent the expansion, and will return a polynomial chaos expansion.
 
     Below, we look at items 2 and 3 in more detail.
 
     <h3>Defining the polynomials and quadrature rules</h3>
         To construct a PCE, we need to know what type of polynomials to use in each input dimension as well as the type of quadrature rule in each input dimension.  In MUQ, this is handles by the Variable and VariableCollection classes.  The Variable class holds a name, quadrature family, and polynomial family and the VariableCollection class holds a bunch of variables - one for each input dimension.  The first step in constructing a PCE is thus to construct a VariableCollection, which can be done in one of two ways.
 
         The first way to construct a VariableCollection is to use the ptree approach defined in the "MUQ/Approximation/utilities/PolychaosXMLHelpers.h" header.  For example, to construct a VariableCollection with the first dimension using a Legendre polynomial and the second dimension using a Hermite polynomial, we could run
 \code{.cpp}
 
 boost::property_tree::ptree subtree_legendre;
 subtree_legendre.put("index", 1); // Use this definition for the first dimension
 subtree_legendre.put("type", "Legendre"); // Use Legendre polynomials and Gauss-Patterson quadrature
 subtree_legendre.put("count", 1); // Use this same definition for only 1 dimension
 
 boost::property_tree::ptree subtree_gaussian;
 subtree_gaussian.put("index", 2); // Use this definition for the second dimension
 subtree_gaussian.put("type", "Gaussian"); // Use Hermite polynomials and Gauss-Hermite quadrature
 subtree_gaussian.put("count", 1); // Use this same definition for only 1 dimension
 
 boost::property_tree::ptree ptree;
 ptree.add_child("Variable", subtree_legendre);
 ptree.add_child("Variable", subtree_gaussian);
 
 auto varCollection = ConstructVariableCollection(ptree);
 
 \endcode
 Similarly, if we wanted to simply use Legendre polynomials for both inputs, we could use
 \code{.cpp}
 #include <MUQ/Approximation/utilities/PolychaosXMLHelpers.h>
 // ...
 
 boost::property_tree::ptree subtree_legendre;
 subtree_legendre.put("index", 1); // Use this definition for the first dimension
 subtree_legendre.put("type", "Legendre"); // Use Legendre polynomials and Gauss-Patterson quadrature
 subtree_legendre.put("count", 2); // Use this same definition twice, for dimension 1 and dimension 2
 
 boost::property_tree::ptree ptree;
 ptree.add_child("Variable", subtree_legendre);
 
 auto varCollection = ConstructVariableCollection(ptree);
 
 // ...
 \endcode
 The advantage of this ptree-based approach is that ptrees can be easily stored in XML files and used for run-time definitions.  For example, the example with one Legendre dimension and one Hermite dimension can be defined in an XML containing:
 \code{.xml}
 
 <Variable>
   <index>1</index>      <!-- Specifies the relative ordering of the variable block -->
   <type>Legendre</type> <!-- Type of variables, Gaussian or Legendre. -->
   <count>1</type>       <!-- How many in this block, defaults to 1-->
 </Variable>
 
 <Variable>
   <index>2</index>      <!-- Specifies the relative ordering of the variable block -->
   <type>Gaussian</type> <!-- Type of variables, Gaussian or Legendre. -->
   <count>1</type>       <!-- How many in this block, defaults to 1-->
 </Variable>
 
 \endcode
 To read this XML file and then construct the variable collection, we could use
 \code{.cpp}
 #include <MUQ/Approximation/utilities/PolychaosXMLHelpers.h>
 #include <boost/property_tree/xml_parser.hpp>
 // ...
 
 boost::property_tree::ptree ptree;
 boost::property_tree::read_xml(filename, ptree);
 auto varCollection = ConstructVariableCollection(ptree);
 
 // ...
 \endcode
 While the ptree-based construction has its advantages, it limits the type of quadrature rules that can be used.  
 
 The second approach to constructing a VariableCollection works with the VariableCollection class directly, skipping the use of the tools in PolychaosXMLHelpers.h.  The VariableCollection class has a function PushVariable that takes a variable name, a quadrature family, and a polynomial family and adds a Variable with those properties to the collection.  For example, constructing the mixed Legendre-Hermite collection from above can also be accomplished with
\code{.cpp}

#include "MUQ/Utilities/VariableCollection.h"
#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussHermiteQuadrature1D.h"
// ...
 
auto varCollection = make_shared<VariableCollection>();
 
auto quadRule1 = make_shared<GaussPattersonQuadrature1D>();
auto polyFam1 = make_shared<LegendrePolynomials1DRecursive>();
varCollection->PushVariable("x1",polyFam1,quadRule1);
 
auto quadRule2 = make_shared<GaussHermiteQuadrature1D>();
auto polyFam2 = make_shared<HermitePolynomials1DRecursive>();
varCollection->PushVariable("x2",polyFam2,quadRule2);
 
// ...
\endcode
This direct approach is also more applicable when the variable collection is used for quadrature alone, and no PCE will be constructed.  In that setting, the polynomial family argument to PushVariable can be removed.  
 
 
<h3>Creating and running a PCE factory</h3>
 
 Once the variable collection has been defined, we can instantiate a class that will actually construct the polynomial chaos expansion.  Assume we have defined a shared_ptr to ModPiece (or cached ModPiece through the CachedModPiece class) and call this ptr fn.  We can create an instance of SmolyakPCEFactory simply with
\code{.cpp}
 #include "MUQ/Approximation/smolyak/SmolyakPCEFactory.h"
 // ... 
 
 auto pceFactory = make_shared<SmolyakPCEFactory>(variableCollection, fn);
 
 // ...
\endcode
 The SmolyakPCEFactory class contains all the machinery to construct the PCE with an adaptive pseudo-spectral method or with a fixed set of terms.  See the class documentation for more details, the code snippets below only show how to construct a PCE by adapting the some tolerance.
  
 Let's say you want to construct a PCE, starting with 2nd order polynomials and adaptiving until the error indicator is less than some tolerance, say 1e-6.  That can be accomplished with something like
 \code{.cpp}
 // ...
 
 int initialOrder = 2;
 double tol = 1e-6;
 shared_ptr<PolynomialChaosExpansion> pce = pceFactory->StartAdaptiveToTolerance(initialOrder, tol);
 
 // ...
 \endcode
 If after StartAdaptiveToTolerance is called, we decide to continue adaptive to a tighter tolerance, we can pick up where we left off by running something like
 \code{.cpp}
 // ...
 
 double tightTol = 1e-10;s
 pce = pceFactory->AdaptToTolerance(tightTol);
 
 // ...
 \endcode
 Note that both of these adapt functions adapt the quadrature and polynomial basis simultaneously.  Additionally, it is also possible to adapt for some fixed length of time with the SmolyakPCEFactory::StartAdaptiveTimed and SmolyakPCEFactory::AdaptForTime functions.  It also possible compute the PCE for a fixed set of terms with the SmolyakPCEFactory::StartFixedTerms function.  All of this functionality is inherited from the SmolyakEstimate class and is shared with the SmolyakQuadrature methods.
 
 Note that the PolynomialChaosExpansion class inherits from muq::Modelling::ModPiece and can therefore be evaluated using the standard Evaluate, Jacobian, etc... methods.
    <h2>Additional information</h2>
 See the examples in MUQ/examples/PolynomialChaos for a complete demonstration of constructing a PCE.
 */


#ifndef SPACEFILLOPTIMIZATION_H_
#define SPACEFILLOPTIMIZATION_H_

#include "MUQ/Optimization/Problems/OptProbBase.h"

namespace muq {
  namespace Approximation {
    
    /// Problem statement to do space filling 
    class SpaceFillOptimization : public muq::Optimization::OptProbBase {
    public:

      /**
	 @param[in] dim The dimension of the points
	 @param[in] neighbors The nearest neighbors
       */
      SpaceFillOptimization(unsigned int const dim, Eigen::MatrixXd const& neighbors);

      /// Objective function 
      /**
	 Maximize the distance from any point (i.e. minimize the negative distance)
	 @param[in] point The input point
       */

    private:

      /// Evaluate the max distance between the point and its neighbors
      /**
	 @param[in] in The point
	 \return Max distance from neighbors
       */
      virtual double eval(Eigen::VectorXd const& in) override;

      /// Evaluate the max distance between the point and its neighbors gradient
      /**
	 @param[in] in The point
	 @param[out] gradient The graident
	 \return Max distance from neighbors
       */
      virtual double grad(Eigen::VectorXd const& in, Eigen::VectorXd& gradient) override;

      const Eigen::MatrixXd neighbors;
    };
  } // namespace Approximation 
} // namespace muq

#endif

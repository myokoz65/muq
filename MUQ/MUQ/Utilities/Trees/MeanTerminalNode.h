
#ifndef _MeanTerminalNode_h
#define _MeanTerminalNode_h

#include "MUQ/Utilities/Trees/BinaryTree.h"


namespace muq {
namespace Utilities {
template<typename ... TrainingTypes>
class MeanTerminalNode : public TerminalNode<Eigen::VectorXd, TrainingTypes ...> {
public:

  static std::shared_ptr < TerminalNode < Eigen::VectorXd, TrainingTypes ... >> Create(const Eigen::MatrixXd & pts,
                                                                                       const muq::Utilities::TreeIndexContainer & inds,
                                                                                       int depth,
                                                                                       boost::property_tree::ptree & properties,
                                                                                       TrainingTypes ... unused)
  {
    auto ptr1 = std::make_shared < MeanTerminalNode < TrainingTypes ... >> (pts, inds, depth, properties);
    auto ptr2 = std::dynamic_pointer_cast < TerminalNode < Eigen::VectorXd, TrainingTypes ... >> (ptr1);

    return ptr2;
  };


  MeanTerminalNode(const Eigen::MatrixXd      & pts,
                   const TreeIndexContainer   & inds,
                   int                          depth,
                   boost::property_tree::ptree& properties)
  {
    mu = Eigen::VectorXd::Zero(pts.rows());
    int N = 0;
    for (auto i = inds.begin(); i != inds.end(); ++i, ++N) {
      mu += pts.col(*i);
    }

    mu /= N;
  }

  virtual Eigen::VectorXd Eval(const Eigen::VectorXd& x) const override
  {
    return mu;
  }

private:

  //REGISTER_TERMINAL_DEC_TYPE(MeanTerminalNode,Eigen::VectorXd)
  static std::shared_ptr < muq::Utilities::DerivedTerminalRegister < Eigen::VectorXd, TrainingTypes ... >> reg;

  Eigen::VectorXd mu;
};
} // namespace Utilities
} // namespace muq

#endif // ifndef _MeanTerminalNode_h

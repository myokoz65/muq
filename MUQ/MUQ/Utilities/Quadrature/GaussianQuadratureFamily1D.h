
#ifndef GAUSSIANQUADRATUREFAMILY1D_H_
#define GAUSSIANQUADRATUREFAMILY1D_H_


#include <boost/serialization/access.hpp>

#include <Eigen/Core>

#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * A virtual class for computing Gaussian quadrature rules of arbitrary order
 * for orthogonal polynomials defined by a recurrence relation. Subclasses
 * provide the necessary specialization for the weight function by providing
 * the recurrence for the associated orthonormal polynomials. Extends the
 * QuadratureFamily1D abstract class.
 *
 * Uses the Golub-Welsch algorithm to compute nodes and weights, taken from
 * Numerical Recipes, ed 3. This algorithm only depends on the monic version
 * of the recurrence relation, which subclasses need to provid, GetMonicCoeff().
 *
 * Subclasses QuadratureFamily1D, which provides caching of quadrature nodes
 * and weights, which avoids repeated calls for the quadrature point
 * from re-solving the eigenproblem given by the Gloub-Welsch algorithm.
 * Therefore, you should use a single instance of this class as widely as
 * possible.
 *
 * Currently only tested for Lagrange and Hermite polynomials, spot checks
 * against Abramowitz and Stegun match almost perfectly up to size 12ish. The test suite
 * checks some values against those stored in text format.
 */
class GaussianQuadratureFamily1D : public QuadratureFamily1D {
public:

  GaussianQuadratureFamily1D();
  virtual ~GaussianQuadratureFamily1D();

  unsigned int GetPrecisePolyOrder(unsigned int const order) const;

  /**
   * Compute a set of nodes and weights using the Golub-Welsch algorithm, using the
   * GetMonicCoeff function to get the coefficients needed for the polynomial family.
   *
   * Assumes that the computation is necessary when called and overwrites the cache.
   *
   * @param order The order of the quadrature rule to compute.
   */
  void         ComputeNodesAndWeights(unsigned int const                   order,
                                      std::shared_ptr<Eigen::RowVectorXd>& nodes,
                                      std::shared_ptr<Eigen::RowVectorXd>& weights) const;

private:

  ///Make class serializable
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);


  ///An abstract function to that returns the monic coefficients of this family.

  /**
   * According to Numerical Recipes, define the three term recurrence as:
   * p_{j+1} = (x-a_j)*(p_j) - b_j*p_{j-1}
   *
   * Provided for the computation of gaussian quadrature rules.
   * http://www.scipy.org/doc/api_docs/SciPy.special.orthogonal.html#gen_roots_and_weights
   * describes the transformation to monic form
   *
   * @param j specifies the desired coefficient
   * @return a rowvec [a_j, b_j]
   */
  virtual Eigen::RowVectorXd GetMonicCoeff(unsigned int const j) const = 0;

  ///A pure virtual function that provides the integral of the weight function over the domain.
  virtual double             IntegralOfWeight() const = 0;
};
}
}

#endif /* GAUSSIANQUADRATUREFAMILY1D_H_ */

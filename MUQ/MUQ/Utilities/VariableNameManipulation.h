
#ifndef VARIABLENAMEMANIPULATION_H_
#define VARIABLENAMEMANIPULATION_H_

#include <vector>
#include <string>

#include <boost/algorithm/string/erase.hpp>

namespace muq {
namespace Utilities {
/// Convert a list (separted by commas) into a vector of std::strings
inline std::vector<std::string> ListToVector(std::string const& list)
{
  std::string::size_type start = 0;
  std::string::size_type end   = list.find(",");

  std::vector<std::string> vec;

  while (end != std::string::npos) {
    vec.push_back(list.substr(start, end - start));
    start = ++end;
    end   = list.find(",", end);
    if (end == std::string::npos) {
      vec.push_back(list.substr(start, list.length()));
    }
  }
  if ((vec.size() == 0) && !list.empty()) { // there is only one element
    vec.push_back(list);
  }

  return vec;
}
}
}

#endif // ifndef VARIABLENAMEMANIPULATION_H_

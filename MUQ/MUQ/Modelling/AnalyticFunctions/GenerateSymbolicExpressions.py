#! /usr/bin/python

import os

# open up the file with all the names in it, each line should have a name, followed by a forward expression, followed by a derivative 
f = open('SymbolicNames.txt')

# read all the names
AllLines = f.readlines()

# loop over lines and generate header files
for line in AllLines:
    words = line.split()
    
    #print 'sed s/FUNCNAME/' + words[0] + '/g ComponentwiseChildBoilerplate.h | sed s/FUNCSTRING/' + words[1] + '/ | sed s/DERIVSTRING/' + words[2] + '/ >'+ words[0] + 'Model.h'
    print 'sed s/FUNCNAME/' + words[0] + '/g ComponentwiseChildBoilerplate.h | sed s/FUNCSTRING/' + words[1] + '/ | sed s/DERIVSTRING/' + words[2] + '/ | sed s/DERIVSECONDSTRING/' + words[3] + '/ >'+ words[0] + 'Model.h'

    # call sed to replace boilerplate code with names
    os.system('sed s/FUNCNAME/' + words[0] + '/g ComponentwiseChildBoilerplate.h | sed s/FUNCSTRING/' + words[1] + '/ | sed s/DERIVSTRING/' + words[2] + '/ | sed s/DERIVSECONDSTRING/' + words[3] + '/ >'+ words[0] + 'Model.h')


# now generate an include file that includes all the expression headers
fout = open('SymbolicHeaders.h','w')
fout.write('#ifndef _SYMBOLICHEADERS_H_\n')
fout.write('#define _SYMBOLICHEADERS_H_\n')
fout.write('//\n//  SymbolicExpressions.h\n//\n//\n//  Created by Matthew Parno on 3/15/12.\n//  Copyright (c) 2012 MIT. All rights reserved.\n//\n\n\n')
fout.write('#include "MUQ/Modelling/AnalyticFunctions/ComponentwiseModel.h"\n')

for line in AllLines:
    words = line.split()

    name = words[0] + 'Model.h'
    fout.write('#include "' + 'MUQ/Modelling/AnalyticFunctions/' + name + '"\n')


#fout.write('\nnamespace muq{\n')
#fout.write('namespace modelling{\n')
#
##fcpp = open('../../../modules/Modelling/src/AnalyticFunctions/SymbolicHeaders.cpp','w')
##fcpp.write('//\n//  SymbolicExpressions.cpp\n//\n//\n//  Created by Matthew Parno on 3/15/12.\n//  Copyright (c) 2012 MIT. All rights reserved.\n//\n\n\n')
##fcpp.write('#include "MUQ/Modelling/AnalyticFunctions/SymbolicHeaders.h"\n\n')
##fcpp.write('using namespace muq::modelling;\n\n')
#
## we also want to generate functions that take expressions as inputs
#fout.write('\n\n\n\n')
#for line in AllLines:
#    words = line.split()
#
#    fout.write('std::shared_ptr<ModPiece> ' + words[1] + '(std::shared_ptr<ModPiece> ModIn);\n\n\n')
#    fcpp.write('std::shared_ptr<ModPiece> muq::modelling::' + words[1] + '(std::shared_ptr<ModPiece> ModIn){\n')
#    fcpp.write('\tstd::shared_ptr<ModPiece> output = std::make_shared<' + words[0] + '>(ModIn.outputSize);\n')
#    fcpp.write('\toutput->SetInput(ModIn,0);\n')
#    fcpp.write('\treturn output;\n')
#    fcpp.write('}\n\n\n')
#
#
#fout.write('}//namespace Modelling \n\n')
#fout.write('}//namespace muq \n\n')

fout.write('#endif')
fout.close()
f.close()
#fcpp.close()


#ifndef MODGRAPHPIECEPYTHON_H_
#define MODGRAPHPIECEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#include "MUQ/Utilities/TwoWayPythonConverter.h"

#include "MUQ/Modelling/python/ModPiecePython.h"
#include "MUQ/Modelling/ModGraphPiece.h"
#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/python/ModGraphPython.h"
#include "MUQ/Modelling/python/DensityPython.h"

namespace muq {
namespace Modelling {
/// Python wrapper class around muq::modelling::ModGraphPiece
class ModGraphPiecePython : public ModGraphPiece, public boost::python::wrapper<ModGraphPiece> {
public:

  /// Create with inputs in default (alphabetical) order
  static std::shared_ptr<ModGraphPiece> PyCreateDefaultOrder(std::shared_ptr<ModGraphPython>& inGraph,
                                                             std::string const              & outNode);

  /// Create with inputs in user specified order
  static std::shared_ptr<ModGraphPiece> PyCreateUserOrder(std::shared_ptr<ModGraphPython>& inGraph,
                                                          std::string const              & outNode,
                                                          boost::python::list const      & inputOrder);

private:
};

void ExportModGraphPiece();

/// Python wrapper class around muq::modelling::ModGraphDensity
class ModGraphDensityPython : public ModGraphDensity, public boost::python::wrapper<ModGraphDensity> {
public:

  /// Create with inputs in default (alphabetical) order
  static std::shared_ptr<ModGraphDensity> PyCreateDefaultOrder(std::shared_ptr<ModGraphPython>& inGraph,
                                                               std::string const              & outNode);

private:
};

void ExportModGraphDensity();
} // namespace modelling
} // namespace muq

#endif // ifndef MODGRAPHPIECEPYTHON_H_

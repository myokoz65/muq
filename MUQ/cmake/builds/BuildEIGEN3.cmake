set(EIGEN_BUILD_DIR "${CMAKE_CURRENT_BINARY_DIR}/external/eigen3/src/EIGEN3")


include(ExternalProject)
if(NOT EIGEN_EXTERNAL_SOURCE)
	set(EIGEN_EXTERNAL_SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/external/eigen-eigen-ffa86ffb5570.zip)
endif()

set(EIGEN3_INSTALL_DIR ${CMAKE_INSTALL_PREFIX}/muq_external/)
ExternalProject_Add(
 EIGEN3
                PREFIX ${CMAKE_CURRENT_BINARY_DIR}/external/eigen3
                URL ${EIGEN_EXTERNAL_SOURCE}
				CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EIGEN3_INSTALL_DIR} -DCMAKE_C_COMPILER=${CMAKE_C_COMPILER} -DCMAKE_CXX_COMPILER=${CMAKE_CXX_COMPILER} -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} -DCMAKE_CXX_FLAGS_DEBUG=${CMAKE_CXX_FLAGS_DEBUG} -DCMAKE_CXX_FLAGS_RELEASE=${CMAKE_CXX_FLAGS_RELEASE} -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
				BUILD_COMMAND make
                INSTALL_COMMAND make install
)




  set_property( TARGET EIGEN3 PROPERTY FOLDER "Externals")

  set(EIGEN3_INCLUDE_DIRS ${EIGEN3_INSTALL_DIR}/include/eigen3)



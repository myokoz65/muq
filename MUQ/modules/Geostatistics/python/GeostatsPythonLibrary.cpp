
#include <boost/python.hpp>

#include "MUQ/Geostatistics/python/CovKernelPython.h"
#include "MUQ/Geostatistics/python/PowerKernelPython.h"

using namespace muq::Geostatistics;

void muq::Geostatistics::ExportCovKernel()
{
  boost::python::class_<CovKernel, std::shared_ptr<CovKernel>, boost::noncopyable> exportCovKernel("CovKernel",
                                                                                                   boost::python::no_init);

  exportCovKernel.def("BuildCov", &CovKernel::PyBuildCovOneInput);
  exportCovKernel.def("BuildCov", &CovKernel::PyBuildCovTwoInputs);
}

void muq::Geostatistics::ExportPowerKernel()
{
  boost::python::class_<PowerKernel, std::shared_ptr<PowerKernel>, boost::python::bases<CovKernel>,
                        boost::noncopyable> exportPowerKernel("PowerKernel", boost::python::init<>());

  // other constructors
  exportPowerKernel.def(boost::python::init<double>());
exportPowerKernel.def(boost::python::init<double, double>());
  exportPowerKernel.def(boost::python::init<double, double, double>());

  // convert to CovKern
  boost::python::implicitly_convertible<std::shared_ptr<PowerKernel>, std::shared_ptr<CovKernel> >();
}

BOOST_PYTHON_MODULE(libmuqGeostatistics)
{
  // Covariance kenals
  muq::Geostatistics::ExportCovKernel();
  muq::Geostatistics::ExportPowerKernel();
}

#include "MUQ/Utilities/Quadrature/ClenshawCurtisQuadrature1D.h"

#include <Eigen/Core>
#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenUtils.h"

using namespace Eigen;
using namespace muq::Utilities;

TEST(UtilitiesQuadrature1DTest, ClenshawCurtis)
{
  ClenshawCurtisQuadrature1D ccQuad;

  unsigned int size = 4;

  std::shared_ptr<RowVectorXd> nodes(ccQuad.GetNodes(size));
  std::shared_ptr<RowVectorXd> weights(ccQuad.GetWeights(size));

  EXPECT_EQ(9,  nodes->cols());
  EXPECT_EQ(9,  weights->cols());
  EXPECT_EQ(9u, ccQuad.GetPrecisePolyOrder(size));
}

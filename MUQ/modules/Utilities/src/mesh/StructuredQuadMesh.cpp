
#include "MUQ/Utilities/mesh/StructuredQuadMesh.h"


#include <cmath>
#include <type_traits>


using namespace muq::Utilities;

/** Default constructor to build an empty mesh with no nodes or elements. */
template<unsigned int dim>
StructuredQuadMesh<dim>::StructuredQuadMesh() : Mesh<dim>(true, true) {}

/** The usual 2d constructor taking the x and y bounds and number or elements.*/
template<unsigned int dim>
StructuredQuadMesh<dim>::StructuredQuadMesh(const Eigen::Matrix<double, dim,
                                                                1>& lbIn,
                                            const Eigen::Matrix<double, dim, 1>& ubIn,
                                            const Eigen::Matrix<unsigned int, dim, 1>& NIn) : Mesh<dim>(true,
                                                                                                        true),
                                                                                              N(NIn), lb(lbIn), ub(ubIn)
{
  dx = (ubIn - lbIn);
  for (unsigned int i = 0; i < dim; ++i) {
    dx[i] /= N[i];
  }
}

/** Get the position of the a node in the mesh*/
template<unsigned int dim>
Eigen::Matrix<double, dim, 1> StructuredQuadMesh<dim>::GetNodePos(unsigned int node) const
{
  Eigen::Matrix<double, dim, 1> pos;

  if (dim == 1) {
    pos[0] = lb[0] + node * dx[0];
  } else if (dim == 2) {
    pos[0] = lb[0] + (node % (N[0] + 1)) * dx[0];
    pos[1] = lb[1] + std::floor(node / (N[0] + 1)) * dx[1];
  } else if (dim == 3) {
    unsigned int px = node % (N[0] + 1);
    unsigned int py = ((node - px) / (N[0] + 1)) % (N[1] + 1);
    unsigned int pz = std::floor(node / ((N[0] + 1) * (N[1] + 1)));

    pos[0] = lb[0] + px * dx[0];
    pos[1] = lb[1] + py * dx[1];
    pos[2] = lb[2] + pz * dx[2];
  }

  return pos;
}

/** Get the geometric center of an element in the mesh.*/
template<unsigned int dim>
Eigen::Matrix<double, dim, 1> StructuredQuadMesh<dim>::GetElePos(unsigned int ele) const
{
  Eigen::Matrix<double, dim, 1> pos;

  if (dim == 1) {
    pos[0] = lb[0] + ele * dx[0] + 0.5 * dx[0];
  } else if (dim == 2) {
    pos[0] = lb[0] + (ele % N[0]) * dx[0] + 0.5 * dx[0];
    pos[1] = lb[1] + std::floor(ele / N[0]) * dx[1] + 0.5 * dx[1];
  } else if (dim == 3) {
    unsigned int px = lb[0] + ele % N[0];
    unsigned int py = lb[1] + ((ele - px) / N[0]) % (N[1]);
    unsigned int pz = lb[2] + std::floor(ele / ((N[0]) * (N[1])));

    pos[0] = (px + 0.5) * dx[0];
    pos[1] = (py + 0.5) * dx[1];
    pos[2] = (pz + 0.5) * dx[2];
  }

  return pos;
}

/** Return a vector of unsigned integers for the center nodes around an element.*/
template<unsigned int dim>
Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> StructuredQuadMesh<dim>::GetNodes(unsigned int ele) const
{
  Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> nodes(int(pow(2, dim)), 1);


  if (dim == 1) {
    nodes(0, 0) = ele;
    nodes(1, 0) = nodes(0, 0) + 1;
  } else if (dim == 2) {
    unsigned int elex = ele % N[0];
    unsigned int eley = floor(double(ele) / N[0]);
    nodes(0, 0) = elex + eley * (N[0] + 1);
    nodes(1, 0) = nodes(0, 0) + 1;
    nodes(2, 0) = nodes(1, 0) + N[0] + 1;
    nodes(3, 0) = nodes(2, 0) - 1;
  } else if (dim == 3) {
    int elex = ele % N[0];
    int eley = int(floor(double(ele - elex) / N[0])) % N[1];
    int elez = int(floor(double(ele - elex - eley * N[0])) / (N[0] * N[1]));

    nodes(0, 0) = elex + eley * (N[0] + 1) + elez * (N[0] + 1) * (N[1] + 1);
    nodes(1, 0) = nodes(0, 0) + 1;
    nodes(2, 0) = nodes(1, 0) + N[0] + 1;
    nodes(3, 0) = nodes(2, 0) - 1;
    nodes(4, 0) = nodes(0, 0) + (N[0] + 1) * (N[1] + 1);
    nodes(5, 0) = nodes(1, 0) + (N[0] + 1) * (N[1] + 1);
    nodes(6, 0) = nodes(2, 0) + (N[0] + 1) * (N[1] + 1);
    nodes(7, 0) = nodes(3, 0) + (N[0] + 1) * (N[1] + 1);
  }

  return nodes;
}

/** Get the area or volume of an element.*/
template<unsigned int dim>
double StructuredQuadMesh<dim>::EleSize(unsigned int ele) const
{
  return dx.prod();
}

/** Get the number of nodes in the mesh. */
template<unsigned int dim>
unsigned int StructuredQuadMesh<dim>::NumNodes() const
{
  int result = (N + Eigen::Matrix<unsigned int, dim, 1>::Ones()).prod();

  return result;
}

/** Get the number of elements in the mesh. */
template<unsigned int dim>
unsigned int StructuredQuadMesh<dim>::NumEles() const
{
  return N.prod();
}

/** Return the one dimensional locations of the elements. */
template<unsigned int dim>
std::vector<Eigen::VectorXd> StructuredQuadMesh<dim>::GetSeparableMeshes() const
{
  std::vector<Eigen::VectorXd> OutVecs(dim);

  // loop over the dimensions
  for (unsigned int d = 0; d < dim; ++d) {
    // allocate the location vector
    Eigen::VectorXd locs = Eigen::VectorXd::Zero(N[d]);

    // fill in location vector
    for (unsigned int i = 0; i < N[d]; ++i) {
      locs[i] = i * dx[d] + lb[d];
    }

    // add this to the output
    OutVecs[d] = locs;
  }

  return OutVecs;
}

namespace muq {
namespace Utilities {
template class StructuredQuadMesh<1>;
template class StructuredQuadMesh<2>;
template class StructuredQuadMesh<3>;
}
}

#include "MUQ/Utilities/python/HDF5LoggingPython.h"

using namespace std;
using namespace muq::Utilities;

void muq::Utilities::ExportHDF5Logging()
{
  boost::python::class_<HDF5Logging, shared_ptr<HDF5Logging>, boost::noncopyable> exportHDF5Logging(
    "HDF5Logging",
    boost::python::init<>());

  exportHDF5Logging.def("Configure", &HDF5Logging::PyConfigure).staticmethod("Configure");
  exportHDF5Logging.def("WipeBuffers", &HDF5Logging::WipeBuffers).staticmethod("WipeBuffers");
}


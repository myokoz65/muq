import unittest
import numpy as np # math stuff in python
import random

import libmuqUtilities
import libmuqModelling
import libmuqApproximation

class RegressorTest(unittest.TestCase):
    def setUp(self):
        Npts = 100

        # linear portion
        L = [[0.5, 0.1],
             [0.2, 0.8]]

        # input and outputs
        self.inputPts  = [[random.random(), random.random()] for x in range(Npts)]
        self.outputPts = np.array([[0.0, 0.0]]*Npts)

        # generate the outputs
        for i in range(Npts):
            temp = (np.dot(np.array(L), np.array(self.inputPts[i]))).tolist()

            self.outputPts[i,0] = np.exp(temp[0])
            self.outputPts[i,1] = np.cos(temp[1])

        self.outputPts = self.outputPts.tolist()
        
        # test input location
        self.testIn  = [0.0, 0.0]
        self.trueOut = [0.0, 0.0]
        self.trueJac = [[0.0, 0.0], [0.0, 0.0]]

        # true output
        temp = (np.dot(np.array(L), np.array(self.testIn))).tolist()
        self.trueOut[0] = np.exp(temp[0])
        self.trueOut[1] = np.cos(temp[1])

        # true jacobian
        self.trueJac[0][0] = self.trueOut[0] * L[0][0]
        self.trueJac[0][1] = self.trueOut[0] * L[0][1]
        self.trueJac[1][0] = -1.0 * L[1][0] * np.sin(temp[1])
        self.trueJac[1][0] = -1.0 * L[1][1] * np.sin(temp[1])

        self.para = dict()
        
        self.para["PolynomialRegressor.Order"] = 5

    def tearDown(self):
        reg = libmuqApproximation.PolynomialRegressor(2, 2, self.para)

        reg.Fit(self.inputPts, self.outputPts)

        result = reg.Evaluate(self.testIn)

        for i in range(2):
            self.assertAlmostEqual(self.trueOut[i], result[i], 4)

        jac = reg.Jacobian(self.testIn)
        
        for i in range(2):
            for j in range(2):
                self.assertAlmostEqual(self.trueJac[i][j], jac[i][j], 3)

    def testLegendrePoly(self):
        self.para["PolynomialRegressor.Basis"] = "Legendre"

    def testHermitePoly(self):
        self.para["PolynomialRegressor.Basis"] = "Hermite"

class TestLinearFunc(libmuqModelling.OneInputFullModPiece):
    def __init__(self):
        libmuqModelling.OneInputFullModPiece.__init__(self, 2, 3)

    def EvaluateImpl(self, input):
        return [1.5, 0.6 * input[0], -0.2 + 2.0 * input[0] - 0.3 * input[1]]

class TestQuadraticFunc(libmuqModelling.OneInputFullModPiece):
    def __init__(self):
        libmuqModelling.OneInputFullModPiece.__init__(self, 2, 3)

    def EvaluateImpl(self, input):
        return [1.5, input[0], -1.0 + input[1] * input[0] - input[1] * input[1]]

class LowOrderRegressorTest(unittest.TestCase):
    def testLinear(self):
        fn = TestLinearFunc()

        para = dict()
        
        para["PolynomialRegressor.Order"] = 1

        reg = libmuqApproximation.PolynomialRegressor(2, 3, para)

        xs = [[0.5268, 0.4655], [1.4282, -0.4213], [0.2282, 0.0696], [-0.3005, -1.7138], [-1.0143, 1.0023], [0.0701, -0.9914]]

        ys = [None]*len(xs)

        for i in range(len(xs)):
            ys[i] = fn.Evaluate(xs[i])

        center = [0.3, 0.1]

        reg.Fit(xs, ys, center)

        testPoint = [0.1, 0.2]

        result = reg.Evaluate(testPoint)

        truth = fn.Evaluate(testPoint)

        self.assertEqual(len(result), len(truth))
        for i in range(len(truth)):
            self.assertAlmostEqual(result[i], truth[i], 12)

        reg.CrossValidationOn()
        
        reg.Fit(xs, ys, center)

        result = reg.Evaluate(testPoint)

        self.assertEqual(len(result), len(truth))
        for i in range(len(truth)):
            self.assertAlmostEqual(result[i], truth[i], 12)

        cvResult = reg.CrossValidationEvaluate(testPoint) 

        for i in range(len(cvResult)):
            self.assertEqual(len(cvResult[i]), len(truth))
            for j in range(len(truth)):
                self.assertAlmostEqual(cvResult[i][j], truth[j], 12)

    def testQuadratic(self):
        fn = TestQuadraticFunc()

        para = dict()
        
        para["PolynomialRegressor.Order"] = 2

        reg = libmuqApproximation.PolynomialRegressor(2, 3, para)

        xs = [[0.5268, 0.4655], [1.4282, -0.4213], [0.2282, 0.0696], [-0.3005, -1.7138], [-1.0143, 1.0023], [0.0701, -0.9914], [-1.0109, -0.2807], [-0.3321, 0.1699], [-0.4165, 0.8949], [-0.0473, -0.8010]]

        ys = [None]*len(xs)

        for i in range(len(xs)):
            ys[i] = fn.Evaluate(xs[i])

        center = [0.3, 0.1]

        reg.Fit(xs, ys, center)

        testPoint = [0.1, 0.2]

        result = reg.Evaluate(testPoint)

        truth = fn.Evaluate(testPoint)

        self.assertEqual(len(result), len(truth))
        for i in range(len(truth)):
            self.assertAlmostEqual(result[i], truth[i], 12)

        reg.CrossValidationOn()
        
        reg.Fit(xs, ys, center)

        result = reg.Evaluate(testPoint)

        self.assertEqual(len(result), len(truth))
        for i in range(len(truth)):
            self.assertAlmostEqual(result[i], truth[i], 12)

        cvResult = reg.CrossValidationEvaluate(testPoint) 

        for i in range(len(cvResult)):
            self.assertEqual(len(cvResult[i]), len(truth))
            for j in range(len(truth)):
                self.assertAlmostEqual(cvResult[i][j], truth[j], 12)

        
    

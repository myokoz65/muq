#include <boost/property_tree/ptree.hpp> // needed here to avoid weird osx "toupper" bug when compiling MUQ with python

#include <stddef.h>
#include <iostream>

#include <boost/math/constants/constants.hpp>

#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/ProbabilistHermite.h"
#include "MUQ/Utilities/Quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussProbabilistHermiteQuadrature1D.h"
#include "MUQ/Utilities/VariableCollection.h"
#include "MUQ/Modelling/CachedModPiece.h"

#include "MUQ/Approximation/pce/PolynomialChaosExpansionTest.h"
#include "MUQ/Approximation/smolyak/SmolyakPCEFactory.h"
#include "MUQ/Approximation/smolyak/SmolyakQuadrature.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

TEST(SmolyakPCE, adaptiveRegressionConstantine1)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //Try a mixed integration type
  varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);

  auto rawFn = make_shared<WrapVectorFunctionModPiece>(pceTestConstantine1, 1, 2);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakPCEFactory pceFactory(varCollection, fn);

  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(1, 1e-4);

  VectorXd point1(1);
  VectorXd point2(1);
  VectorXd point3(1);
  VectorXd point4(1);

  //two arbitrary points to test
  point1 << .9;       //, .22;
  point2 << -.3;      //, .5;
  point3 << .58;      //, .7;
  point4 << -.192747; // , -.4;

  //check the values against stored ones that came from this code - regression
  VectorXd result = pceTestConstantine1(point1);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point1), 1e-3));
  result = pceTestConstantine1(point2);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point2), 1e-3));
  result = pceTestConstantine1(point3);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point3), 1e-3));
  result = pceTestConstantine1(point4);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point4), 1e-3));
}


TEST(SmolyakPCE, adaptiveRegressionConstantine2)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //Try a mixed integration type
  varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);
  varCollection->PushVariable("y", legendrePoly1D, legendreQuad1D);

  auto rawFn = make_shared<WrapVectorFunctionModPiece>(pceTestConstantine2, 2, 2);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakPCEFactory pceFactory(varCollection, fn);
  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(1, .01);

  VectorXd point1(2);
  VectorXd point2(2);
  VectorXd point3(2);
  VectorXd point4(2);

  //two arbitrary points to test
  point1 << .9, .22;
  point2 << -.3, .5;
  point3 << .58, .7;
  point4 << -.192747, -.4;

  //check the values against stored ones that came from this code - regression
  VectorXd result = pceTestConstantine2(point1);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point1), 4e-2));
  result = pceTestConstantine2(point2);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point2), 8e-2));
  result = pceTestConstantine2(point3);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point3), 5e-2));
  result = pceTestConstantine2(point4);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point4), 2e-1));

  double   dx = 0.0000001;
  VectorXd perturbationX(2);
  perturbationX << dx, 0;
  VectorXd perturbationY(2);
  perturbationY << 0, dx;
  Matrix2d jacobian;
  jacobian.col(0) = (pceTestConstantine2(point1 + perturbationX) - pceTestConstantine2(point1)).array() / dx;
  jacobian.col(1) = (pceTestConstantine2(point1 + perturbationY) - pceTestConstantine2(point1)).array() / dx;
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, jacobian, pce->Jacobian(point1), 0.03);
}

TEST(SmolyakPCE, adaptiveRegressionConstantine3)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //Try a mixed integration type
  varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);
  varCollection->PushVariable("y", legendrePoly1D, legendreQuad1D);

  auto rawFn = make_shared<WrapVectorFunctionModPiece>(pceTestConstantine3, 2, 2);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakPCEFactory pceFactory(varCollection, fn);

  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(1, 1e-3);

  VectorXd point1(2);
  VectorXd point2(2);
  VectorXd point3(2);
  VectorXd point4(2);

  //two arbitrary points to test
  point1 << .9, .22;
  point2 << -.3, .5;
  point3 << .58, .7;
  point4 << -.192747, -.4;

  //check the values against stored ones that came from this code - regression
  VectorXd result = pceTestConstantine3(point1);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point1), 1e-3));
  result = pceTestConstantine3(point2);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point2), 1e-3));
  result = pceTestConstantine3(point3);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point3), 1e-3));
  result = pceTestConstantine3(point4);
  EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point4), 1e-3));
}

// TEST(SmolyakPCE, adaptiveRegressionConstantine3StartStop)
// {
//   GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
//   LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());
// 
//   VariableCollection::Ptr varCollection(new VariableCollection());
// 
//   //Try a mixed integration type
//   varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);
//   varCollection->PushVariable("y", legendrePoly1D, legendreQuad1D);
// 
// 
//   auto rawFn = make_shared<WrapVectorFunctionModPiece>(pceTestConstantine3, 2, 2);
//   auto fn    = make_shared<CachedModPiece>(rawFn);
//   SmolyakPCEFactory::Ptr pceFactory(new SmolyakPCEFactory(varCollection, fn));
// 
//   PolynomialChaosExpansion::Ptr pce = pceFactory->StartAdaptiveToTolerance(1, 1e-4);
// 
//   //    BOOST_LOG_POLY("smolyak",debug) << "Starting to save ";
// 
//   //    SmolyakPCEFactory::SaveProgress(pceFactory, "results/tests/adaptiveRegressionConstantine3StartStop");
// 
//   //    BOOST_LOG_POLY("smolyak",debug) << "Progress saved. ";
// 
//   //    SmolyakPCEFactory::Ptr pceFactory2 =
//   // SmolyakPCEFactory::LoadProgress("results/tests/adaptiveRegressionConstantine3StartStop",
//   //                    &pceTestConstantine3);
// 
//   //    pce = pceFactory2->AdaptToTolerance(1e-4);
// 
//   PolynomialChaosExpansion::SaveToFile(pce, "results/tests/pce.dat");
// 
//   PolynomialChaosExpansion::Ptr pceCopy = PolynomialChaosExpansion::LoadFromFile("results/tests/pce.dat");
// 
//   VectorXd point1(2);
//   VectorXd point2(2);
//   VectorXd point3(2);
//   VectorXd point4(2);
// 
//   //two arbitrary points to test
//   point1 << .9, .22;
//   point2 << -.3, .5;
//   point3 << .58, .7;
//   point4 << -.192747, -.4;
// 
//   //check the values against stored ones that came from this code - regression
//   VectorXd result = pceTestConstantine3(point1);
//   EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point1), 1e-3));
//   result = pceTestConstantine3(point2);
//   EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point2), 1e-3));
//   result = pceTestConstantine3(point3);
//   EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point3), 1e-3));
//   result = pceTestConstantine3(point4);
//   EXPECT_TRUE(MatrixApproxEqual(result, pce->Evaluate(point4), 1e-3));
// 
//   EXPECT_TRUE(MatrixApproxEqual(pce->Evaluate(point4), pceCopy->Evaluate(point4), 1e-14));
// }

TEST(SmolyakPCE, addSpecificPolynomials)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //Try a mixed integration type
  varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);
  varCollection->PushVariable("y", legendrePoly1D, legendreQuad1D);

  auto rawFn = make_shared<WrapVectorFunctionModPiece>(pceTestConstantine3, 2, 2);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakPCEFactory::Ptr pceFactory(new SmolyakPCEFactory(varCollection, fn));

  MatrixXu polys(2, 2);
  polys << 8, 1, 0, 4;

  PolynomialChaosExpansion::Ptr pce = pceFactory->ComputeFixedPolynomials(polys);

  RowVectorXu poly1(2), poly2(2), poly3(2);
  poly1 << 8, 1;
  poly2 << 0, 4;
  poly3 << 9, 9;
  EXPECT_TRUE(pce->IsPolynomialInExpansion(poly1));
  EXPECT_TRUE(pce->IsPolynomialInExpansion(poly2));
  EXPECT_FALSE(pce->IsPolynomialInExpansion(poly3));
}


VectorXd IshigamiPolySensitivityIndexFn(VectorXd const& input)
{
  //This is a test function from Polynomial chaos expansion for sensitivity analysis,
  //Crestaux, Le Maitre, Martinez 2009.

  VectorXd firstStep = input.array() * boost::math::constants::pi<double>();

  double a = 1.0, b = 2.0;

  VectorXd result(2);
  double   term2 = sin(firstStep(1));

  result(0) = sin(firstStep(0)) + a * term2 * term2 + b * sin(firstStep(0)) * firstStep(2) * firstStep(2) *
              firstStep(2) * firstStep(2);

  double c     = 3.0, d = 4.0;
  double term3 = sin(firstStep(1));
  result(1) = sin(firstStep(0)) + c * term3 * term3 + d * sin(firstStep(0)) * firstStep(2) * firstStep(2) *
              firstStep(2) * firstStep(2);

  return result;
}

///Test the variance and sensitivity estimates against known results
TEST(SmolyakPCE, testSensitivityEstimates)
{
  //This is a test function from Polynomial chaos expansion for sensitivity analysis,
  //Crestaux, Le Maitre, Martinez 2009.

  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());


  unsigned inputDim = 3;


  for (unsigned i = 0; i < inputDim; ++i) {
    varCollection->PushVariable("x", legendrePoly1D, pattersonQuad1D);
  }

  auto fn = make_shared<WrapVectorFunctionModPiece>(IshigamiPolySensitivityIndexFn, inputDim, 2);


  SmolyakPCEFactory pceFactory(varCollection, fn);

  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(3, 1e-4);


  double pi = boost::math::constants::pi<double>();
  double a  = 1.0, b = 2.0;
  double c  = 3.0, d = 4.0;

  VectorXd variance(2);
  variance(0) = a * a / 8.0 + b / 5.0 * pow(pi, 4) + b * b * pow(pi, 8) / 18.0 + .5;
  variance(1) = c * c / 8.0 + d / 5.0 * pow(pi, 4) + d * d * pow(pi, 8) / 18.0 + .5;

  MatrixXd mainEffects(2, 3);
  mainEffects <<  b / 5.0 *
    pow(pi,
        4) + b * b *
    pow(pi, 8) / 50.0 + .5, a * a / 8, 0, d / 5.0 * pow(pi, 4) + d * d * pow(pi, 8) / 50.0 + .5, c * c / 8, 0;
  mainEffects.row(0) /= variance(0); //note that the paper doesn't normalize by the variance
  mainEffects.row(1) /= variance(1);

  MatrixXd totalEffects(2, 3);
  totalEffects <<  b / 5.0 *
    pow(pi,
        4) + b * b *
    pow(pi,
        8) / 18.0 + .5, a * a / 8,
  b *b *pow(
    pi,
    8) / 18.0 - b * b * pow(pi, 8) / 50.0, d / 5.0 * pow(pi,
                                                         4)
  + d * d * pow(pi, 8) / 18.0 + .5, c * c / 8, d *d *pow(pi, 8) / 18.0 - d * d * pow(pi, 8) / 50.0;
  totalEffects.row(0) /= variance(0);
  totalEffects.row(1) /= variance(1);

  EXPECT_TRUE(MatrixApproxEqual(variance, pce->ComputeVariance(), 1e-5));
  EXPECT_TRUE(MatrixApproxEqual(mainEffects,  pce->ComputeAllMainSensitivityIndices(), 1e-5));
  EXPECT_TRUE(MatrixApproxEqual(totalEffects, pce->ComputeAllSobolTotalSensitivityIndices(), 1e-5));
}


VectorXd SimpleExpExample(VectorXd const& input)
{
  return exp(input[0] + 0.2 * input[1]) * Eigen::VectorXd::Ones(1);
}

VectorXd SimpleSumExample(VectorXd const& input)
{
  return (0.1 + input.sum()) * Eigen::VectorXd::Ones(1);
}

TEST(SmolyakPCE, testGaussianVarianceEstimateHermite)
{
  auto hermiteQuad1D = make_shared<GaussHermiteQuadrature1D>();
  HermitePolynomials1DRecursive::Ptr hermitePoly1D(new HermitePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());


  unsigned inputDim = 3;


  for (unsigned i = 0; i < inputDim; ++i) {
    varCollection->PushVariable("x", hermitePoly1D, hermiteQuad1D);
  }


  auto fn = make_shared<WrapVectorFunctionModPiece>(SimpleSumExample, inputDim, 1);


  SmolyakPCEFactory pceFactory(varCollection, fn);

  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(1, 1e-6);


  EXPECT_NEAR(0.5 * inputDim, pce->ComputeVariance()[0], 1e-5);
  EXPECT_NEAR(0.1,            pce->ComputeMean()[0],     1e-5);
}

TEST(SmolyakPCE, testGaussianVarianceEstimateProbHermite)
{
  auto hermiteQuad1D = make_shared<GaussProbabilistHermiteQuadrature1D>();
  auto hermitePoly1D = make_shared<ProbabilistHermite>();
  
  VariableCollection::Ptr varCollection(new VariableCollection());
  
  
  unsigned inputDim = 3;
  
  
  for (unsigned i = 0; i < inputDim; ++i) {
    varCollection->PushVariable("x", hermitePoly1D, hermiteQuad1D);
  }
  
  auto fn = make_shared<WrapVectorFunctionModPiece>(SimpleSumExample, inputDim, 1);
  
  
  SmolyakPCEFactory pceFactory(varCollection, fn);
  
  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(1, 1e-2);
  
  
  EXPECT_NEAR(inputDim, pce->ComputeVariance()[0], 1e-5);
  EXPECT_NEAR(0.1,            pce->ComputeMean()[0],     1e-5);
}


TEST(SmolyakPCE, testVarianceEstimateHermite)
{
  GaussHermiteQuadrature1D::Ptr hermiteQuad1D(new GaussHermiteQuadrature1D());
  HermitePolynomials1DRecursive::Ptr hermitePoly1D(new HermitePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());


  unsigned inputDim = 2;


  for (unsigned i = 0; i < inputDim; ++i) {
    varCollection->PushVariable("x", hermitePoly1D, hermiteQuad1D);
  }


  auto fn = make_shared<WrapVectorFunctionModPiece>(SimpleExpExample, inputDim, 1);


  SmolyakPCEFactory pceFactory(varCollection, fn);

  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(5, 1e-5);


  EXPECT_NEAR(1.069560557758917, pce->ComputeVariance()[0], 1e-1);
  EXPECT_NEAR(1.284025416687741, pce->ComputeMean()[0],     1e-1);
}

TEST(SmolyakPCE, testUniformVarianceEstimateLegendre)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());


  unsigned inputDim = 3;


  for (unsigned i = 0; i < inputDim; ++i) {
    varCollection->PushVariable("x", legendrePoly1D, pattersonQuad1D);
  }

  auto fn = make_shared<WrapVectorFunctionModPiece>(SimpleSumExample, inputDim, 1);


  SmolyakPCEFactory pceFactory(varCollection, fn);

  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(3, 1e-8);


  EXPECT_NEAR(pow(2, inputDim) * pow(0.5, inputDim), pce->ComputeVariance()[0], 1e-5);
  EXPECT_NEAR(0.1,                                   pce->ComputeMean()[0],     1e-5);
}

TEST(SmolyakPCE, testVarianceEstimateLegendre)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());


  unsigned inputDim = 2;


  for (unsigned i = 0; i < inputDim; ++i) {
    varCollection->PushVariable("x", legendrePoly1D, pattersonQuad1D);
  }

  auto fn = make_shared<WrapVectorFunctionModPiece>(SimpleExpExample, inputDim, 1);


  SmolyakPCEFactory pceFactory(varCollection, fn);

  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(5, 1e-5);


  EXPECT_NEAR(0.462565708868450, pce->ComputeVariance()[0], 3e-2);
  EXPECT_NEAR(1.183051552548825, pce->ComputeMean()[0],     3e-2);
}


template<int NumOut>
VectorXd SimpleQuadExample(VectorXd const& input)
{
  return input.squaredNorm() * Eigen::VectorXd::Ones(NumOut);
}

TEST(SmolyakPCE, varianceJacobianCubic)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());


  unsigned inputDim = 4;

  for (unsigned i = 0; i < inputDim; ++i) {
    varCollection->PushVariable("x", legendrePoly1D, pattersonQuad1D);
  }

  auto fn = make_shared<WrapVectorFunctionModPiece>(SimpleQuadExample<2>, inputDim, 2);


  SmolyakPCEFactory pceFactory(varCollection, fn);

  PolynomialChaosExpansion::Ptr pce = pceFactory.StartAdaptiveToTolerance(4, 1e-9);
  
  Eigen::VectorXd testPt = Eigen::VectorXd::Constant(inputDim,0.5);
  Eigen::VectorXd trueEval = SimpleQuadExample<2>(testPt);
  Eigen::VectorXd testEval = pce->Evaluate(testPt);
  EXPECT_NEAR(trueEval(0),testEval(0),1e-3);
  EXPECT_NEAR(trueEval(1),testEval(1),1e-3);
  
  Eigen::MatrixXd Jac = pce->ComputeVarianceJacobian();
  
  Eigen::RowVectorXu testMulti = Eigen::RowVectorXu::Zero(inputDim);
  testMulti(0) = 2;
  
  int testInd = pce->GetMultiSet()->MultiToIndex(testMulti);
  EXPECT_NEAR(0.190476, Jac(0, testInd), 1e-1);
  EXPECT_NEAR(0.190476, Jac(1, testInd), 1e-1);
}

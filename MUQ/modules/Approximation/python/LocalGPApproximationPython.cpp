#include "MUQ/Approximation/python/LocalGPApproximationPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

LocalGPApproximation::LocalGPApproximation(shared_ptr<ModPiece> model, py::dict const& dict) :
  LocalGPApproximation(model, PythonDictToPtree(dict)) {}

LocalGPApproximation::RefinementStatus LocalGPApproximation::PyRefineNear(boost::python::list const& input)
{
  return RefineNear(GetEigenVector<Eigen::VectorXd>(input));
}

LocalGPApproximation::RefinementStatus LocalGPApproximation::PyRefineWithPoint(boost::python::list const& input)
{
  return RefineWithPoint(GetEigenVector<Eigen::VectorXd>(input));
}

void muq::Approximation::ExportLocalGPApproximation()
{
  py::enum_<LocalGPApproximation::Modes> modes("LocalGPApproximationMode");

  modes.value("standard", LocalGPApproximation::standard);
  modes.value("crossValidation", LocalGPApproximation::crossValidation);
  modes.value("trueDirect", LocalGPApproximation::trueDirect);
  modes.value("nugget", LocalGPApproximation::nugget);

  py::enum_<LocalGPApproximation::RefinementStatus> status("LocalGPApproximationStatus");

  status.value("success", LocalGPApproximation::success);
  status.value("successNoContinue", LocalGPApproximation::successNoContinue);
  status.value("failure", LocalGPApproximation::failure);

  py::class_<LocalGPApproximation, shared_ptr<LocalGPApproximation>, boost::noncopyable,
             py::bases<ModPiece> > exportGPA(
    "LocalGPApproximation",
    py::init<shared_ptr<ModPiece>, py::dict const&>());

  exportGPA.def("SetMode", &LocalGPApproximation::SetMode);
  exportGPA.def("GetNumberOfCrossValidationFits", &LocalGPApproximation::GetNumberOfCrossValidationFits);
  exportGPA.def("RefineNear", &LocalGPApproximation::PyRefineNear);
  exportGPA.def("RefineWithPoint", &LocalGPApproximation::PyRefineWithPoint);
  exportGPA.def("GetNumberOfAvailableSamples", &LocalGPApproximation::GetNumberOfAvailableSamples);
  exportGPA.def("GetMinimumNumberOfSamples", &LocalGPApproximation::GetMinimumNumberOfSamples);

  py::implicitly_convertible<shared_ptr<LocalGPApproximation>, shared_ptr<ModPiece> >();
}


#include "MUQ/Approximation/python/RegressorBasePython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

shared_ptr<RegressorBase> RegressorBase::PyCreate(int dimIn, int dimOut, py::dict const& dict) 
{
  boost::property_tree::ptree pt = PythonDictToPtree(dict);

  return RegressorBase::Create(dimIn, dimOut, pt);
}

shared_ptr<RegressorBase> RegressorBase::PyCreateNoOutDim(int dimIn, py::dict const& dict) 
{
  boost::property_tree::ptree pt = PythonDictToPtree(dict);

  return RegressorBase::Create(dimIn, 1, pt);
}

void RegressorBase::PyFit(boost::python::list const& xs, boost::python::list const& ys) 
{
  Fit(GetEigenMatrix(xs).transpose(), GetEigenMatrix(ys));
}

void RegressorBase::PyFitWithCenter(py::list const& xs,
				    py::list const& ys,
				    py::list const& center, 
				    double const radius) 
{
  return FitWithCenter(GetEigenMatrix(xs).transpose(), 
		       GetEigenMatrix(ys).transpose(), 
		       GetEigenVector<Eigen::VectorXd>(center), 
		       radius);
}

void RegressorBase::PyFitWithCrossValidationWithCenter(py::list const& xs,
						       py::list const& ys,
						       py::list const& center, 
						       double const radius) 
{
  return FitWithCrossValidationWithCenter(GetEigenMatrix(xs).transpose(), 
					  GetEigenMatrix(ys).transpose(), 
					  GetEigenVector<Eigen::VectorXd>(center), 
					  radius);
}

void muq::Approximation::ExportRegressorBase() 
{
  py::class_<RegressorBase, shared_ptr<RegressorBase>, py::bases<ModPiece>, boost::noncopyable> exportRegressor("RegressorBase", py::no_init);

  exportRegressor.def("__init__", py::make_constructor(&RegressorBase::PyCreate));
  exportRegressor.def("__init__", py::make_constructor(&RegressorBase::PyCreateNoOutDim));

  exportRegressor.def("Fit", &RegressorBase::PyFit);
  exportRegressor.def("FitWithCenter", &RegressorBase::PyFitWithCenter);
  exportRegressor.def("FitWithCrossValidationWithCenter", &RegressorBase::PyFitWithCrossValidationWithCenter);

  py::implicitly_convertible<shared_ptr<RegressorBase>, shared_ptr<ModPiece> >();
}

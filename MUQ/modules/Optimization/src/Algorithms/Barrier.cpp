
#include "MUQ/Optimization/Algorithms/Barrier.h"
#include "MUQ/Optimization/Problems/BarrierProb.h"

using namespace muq::Optimization;
using namespace std;

REGISTER_OPT_DEF_TYPE(Barrier) Barrier::Barrier(std::shared_ptr<OptProbBase> ProbPtr,
                                                boost::property_tree::ptree& properties) : OptAlgBase(ProbPtr,
                                                                                                      properties,
                                                                                                      false,
                                                                                                      true,
                                                                                                      false,
                                                                                                      true,
                                                                                                      true,
                                                                                                      false,
                                                                                                      true,
                                                                                                      false),
                                                                                           uncProperties(properties)
{
  // get penalty specific parameters
  barrierCoeff = properties.get("Opt.Barrier.StartCoeff", 1);
  barrierScale = properties.get("Opt.Barrier.CoeffScale", 0.5);
  maxOuterIts  = properties.get("Opt.Barrier.OuterIts", 100);

  assert(barrierCoeff > 0.0);
  assert(barrierScale < 1.0);
}

/** Solve the optimization problem using x0 as a starting point for the iteration. */
Eigen::VectorXd Barrier::solve(const Eigen::VectorXd& x0)
{
  // current position
  Eigen::VectorXd xc = x0;

  bool   optimalFlag = false;
  double dx          = 10;
  int    it          = 0;

  while ((!optimalFlag) && (it < maxOuterIts)) {
    if (verbose > 0) {
      std::cout << "Solving Barrier subproblem...\n";
      std::cout << "\tOuter iteration " << it << std::endl;
    }

    // created the penalized optimization problem
    shared_ptr<OptProbBase> BarProb = make_shared<BarrierProb>(OptProbPtr, barrierCoeff);

    // create a unconstrained solver for this problem
    shared_ptr<OptAlgBase> uncSolver = OptAlgBase::Create(BarProb, uncProperties);


    // solve the unconstrainted problem
    Eigen::VectorXd newX = uncSolver->solve(xc);
    status = uncSolver->GetStatus();
    if (verbose > 0) {
      std::cout << "\tInner iteration terminated with status " << status << std::endl;
    }

    // if we had a successful solve, update the current position to the min of the unconstrained problem
    if (status >= 0) {
      // check to see how much we've changed
      dx = (newX - xc).norm();

      // update the current iterate with the results of the subproblem optimization
      xc = newX;
    } else {
      // we failed, so just return what we have and leave the failed status flag
      return newX;
    }

    // check for convergence in equality constraints
    Eigen::VectorXd inVals;

    // check for convergence in inequality constraints and update mu if we need to continue
    if ((OptProbPtr->NumInequalities() > 0) && (verbose > 0)) {
      inVals = OptProbPtr->inequalityConsts.eval(xc);

      if (verbose > 0) {
        std::cout << "\tCurrent inequality residual: " << inVals.maxCoeff() << std::endl << std::endl;
      }
    }

    // if we are not feasible, but haven't changed position much, let's say we are "optimal"
    if (dx < xtol) {
      optimalFlag = true;
    }

    // update the penalty parameter
    barrierCoeff *= barrierScale;

    ++it;
  }

  return xc;
}

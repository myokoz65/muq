
#include "MUQ/Optimization/Algorithms/SD.h"

using namespace muq::Optimization;


REGISTER_OPT_DEF_TYPE(SD_Line)
/** Compute the SD descent direction, update the gradient, and update fold */
Eigen::VectorXd SD_Line::step()
{
  // first, evaluate the objective and gradient at the current iterate
  fold = OptProbPtr->grad(xc, gc);

  // compute and return the descent direction scaled by the step length
  return -1.0 * stepLength * gc;
}


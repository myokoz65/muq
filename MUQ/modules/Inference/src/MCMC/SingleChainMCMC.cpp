#include "MUQ/Inference/MCMC/SingleChainMCMC.h"

#include <assert.h>
#include <iostream>
#include <cmath>

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/LogConfig.h"
#include <MUQ/Modelling/EmpiricalRandVar.h>
#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Utilities/HDF5Logging.h"
#include <MUQ/Utilities/HDF5Wrapper.h>

// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

REGISTER_MCMCBASE(SingleChainMCMC);

SingleChainMCMC::SingleChainMCMC(std::shared_ptr<MCMCKernel>  kernelIn,
                                 boost::property_tree::ptree& properties) : MCMCBase(kernelIn->samplingProblem,
                                                                                     properties), transitionKernel(
														   kernelIn), storeESS(properties.get("MCMC.StoreESS", false))
{
  LOG(INFO) << "Constructing SingleChainMCMC";
  posteriorSamples = make_shared<EmpiricalRandVar>(samplingProblem->samplingDim);
  
  bool enableLogging = properties.get("MCMC.EnableLogging",true);
  if(enableLogging){
    hdf5Group = properties.get("MCMC.HDF5OutputGroup", "");
    LOG(INFO) << "Writing to HDF5 in MCMC.HDF5OutputGroup = " << hdf5Group;
  }else{
    hdf5Group = "";
    LOG(INFO) << "Disabling HDF5 logging because MCMC.EnableLogging = " << enableLogging;
  }
  
  outputDetailsToHDF5 = ReadAndLogParameter(properties, "MCMC.HDF5OutputDetails", outputDetailsToHDF5, hdf5Group);
  LOG(INFO) << "Writing extra details to HDF5? MCMC.HDF5OutputDetails = " << outputDetailsToHDF5;
}

SingleChainMCMC::SingleChainMCMC(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                                 boost::property_tree::ptree            & properties) : SingleChainMCMC(MCMCKernel::Create(
                                                                                                          samplingProblem,
                                                                                                          properties),
                                                                                                        properties)
{}

SingleChainMCMC::~SingleChainMCMC() {}

void SingleChainMCMC::PrintProgress()
{
  //print overall state
  MCMCBase::PrintProgress();

  //then anything more specific -- kernel state
  if ((currIteration - 1) % (totalIterations / 10) == 0) {
    if (verbose > 1) {
      transitionKernel->PrintStatus();
    }
  }
}

void SingleChainMCMC::SetupStartState(Eigen::VectorXd const& xStart)
{
  assert(samplingProblem);

  //start a timer
  startChain = std::chrono::steady_clock::now();

  //pass the start information to the kernel
  transitionKernel->SetupStartState(xStart);

  //start a log entry
  auto logEntry = make_shared<HDF5LogEntry>();

  currentState = samplingProblem->ConstructState(xStart, 1, logEntry);
  assert(currentState); //start state must be valid

  // preallocate the memory for the posterior samples -- increases speed
  posteriorSamples->ExpectedSize(std::max(int(ceil((totalIterations - burnInLength) / saveSampleFrequency)), 0));

  //if the log is being output and we want detailed logging, capture the forward model value for the current state
  if (UsingHDF5()) {
    if (outputDetailsToHDF5) { //if we want more details, add them in
      if (currentState->HasForwardModel()) {
        logEntry->loggedData["forwardModel"] = currentState->GetForwardModel();
      }
    }

    //and log the current state
    logEntry->loggedData["chain"] = currentState->state;

    //and output the entry
    HDF5Logging::WriteEntry(hdf5Group, logEntry, 0, totalIterations + 1);
  }
}

void SingleChainMCMC::SetCurrentState(std::shared_ptr<MCMCState> newState)
{
  currentState = newState;
}

void SingleChainMCMC::SampleOnce()
{
  assert(currentState);

  //create a log entry for this sampling procedure
  auto logEntry = make_shared<HDF5LogEntry>();

  //construct and assign the new state
  currentState = transitionKernel->ConstructNextState(currentState, currIteration, logEntry);

  assert(currentState); //must have a state new state

  if (IsCurrentIterationSaved()) {
    // store state in posterior distribution
    posteriorSamples->addSamp(currentState->state);
  }

  if (UsingHDF5()) {
    if (outputDetailsToHDF5) {                                                                       //if we want to log
                                                                                                     // more details,
                                                                                                     // add them in
      if (currentState->HasForwardModel()) {
        logEntry->loggedData["forwardModel"] = currentState->GetForwardModel();                      //grab the forward
                                                                                                     // model
      }
      
      //grab the run-time of the chain
      std::chrono::duration<double> chainDuration = std::chrono::steady_clock::now() - startChain;
      logEntry->loggedData["runTime"] = Eigen::VectorXd::Constant(1, std::chrono::duration_cast<std::chrono::milliseconds>(
                                          chainDuration).count() / 1000.0);

      logEntry->loggedData["logDensity"] = Eigen::VectorXd::Constant(1, currentState->GetDensity()); //and the density

      if( storeESS ) {
	logEntry->loggedData["ESS"] = posteriorSamples->getEss(); // the effective sample size
      }
    } else {                                                                                         //else throw away
                                                                                                     // anything the
                                                                                                     // proposal created
      logEntry->loggedData.clear();                                                                  //if we don't want
                                                                                                     // the details,
                                                                                                     // throw away
                                                                                                     // anything the
                                                                                                     // kernel (and
                                                                                                     // called methods)
                                                                                                     // might have put
                                                                                                     // in
    }

    logEntry->loggedData["chain"] = currentState->state;                                             //we always want
                                                                                                     // the state

    HDF5Logging::WriteEntry(hdf5Group, logEntry, currIteration, totalIterations + 1);                //output the log
                                                                                                     // entry

	



    //on the last iteration, write the compute time
    if (currIteration == totalIterations) {
      std::chrono::duration<double> chainDuration = std::chrono::steady_clock::now() - startChain;

      HDF5Wrapper::WriteScalarAttribute(hdf5Group + "/chain", "runTimeSeconds",
                                        std::chrono::duration_cast<std::chrono::milliseconds>(
                                          chainDuration).count() / 1000.0);
    }
  }

  ++currIteration; //update the iteration count
}

void SingleChainMCMC::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  MCMCBase::WriteAttributes(groupName, prefix);
  transitionKernel->WriteAttributes(groupName, prefix);
}

void SingleChainMCMC::WriteAttributes() const
{
  if (UsingHDF5()) {
    WriteAttributes(hdf5Group);
  }
}


#include "MUQ/Inference/python/StaticPolynomialsPython.h"

using namespace std;
using namespace muq::Inference;

void muq::Inference::ExportStaticHermitePoly()
{
  boost::python::class_<StaticHermitePoly, shared_ptr<StaticHermitePoly> > exportStaticHermite("StaticHermitePoly",
                                                                                               boost::python::init<>());

  exportStaticHermite.def("evaluate", &StaticHermitePoly::evaluate).staticmethod("evaluate");
  exportStaticHermite.def("derivative", &StaticHermitePoly::derivative).staticmethod("derivative");
  exportStaticHermite.def("secondDerivative", &StaticHermitePoly::secondDerivative).staticmethod("secondDerivative");
  exportStaticHermite.def("GetMonomialCoeffs", &StaticHermitePoly::PyGetMonomialCoeffs).staticmethod(
    "GetMonomialCoeffs");
}

void muq::Inference::ExportStaticLegendrePoly()
{
  boost::python::class_<StaticLegendrePoly, shared_ptr<StaticLegendrePoly> > exportStaticLegendre("StaticLegendrePoly",
                                                                                                  boost::python::init<>());

  exportStaticLegendre.def("evaluate", &StaticLegendrePoly::evaluate).staticmethod("evaluate");
  exportStaticLegendre.def("derivative", &StaticLegendrePoly::derivative).staticmethod("derivative");
  exportStaticLegendre.def("secondDerivative", &StaticLegendrePoly::secondDerivative).staticmethod("secondDerivative");
  exportStaticLegendre.def("GetMonomialCoeffs", &StaticLegendrePoly::PyGetMonomialCoeffs).staticmethod(
    "GetMonomialCoeffs");
}

void muq::Inference::ExportStaticMonomial()
{
  boost::python::class_<StaticMonomial, shared_ptr<StaticMonomial> > exportStaticHermite("StaticMonomial",
                                                                                         boost::python::init<>());

  exportStaticHermite.def("evaluate", &StaticMonomial::evaluate).staticmethod("evaluate");
  exportStaticHermite.def("derivative", &StaticMonomial::derivative).staticmethod("derivative");
  exportStaticHermite.def("secondDerivative", &StaticMonomial::secondDerivative).staticmethod("secondDerivative");
  exportStaticHermite.def("GetMonomialCoeffs", &StaticMonomial::PyGetMonomialCoeffs).staticmethod(
    "GetMonomialCoeffs");
}


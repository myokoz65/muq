#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/mesh/StructuredQuadMesh.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Modelling/LinearModel.h"
#include "MUQ/Modelling/GaussianPair.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Modelling/DensityProduct.h"
#include "MUQ/Modelling/SumModel.h"

#include "MUQ/Inference/MCMC/MCMCBase.h"
#include "MUQ/Inference/ProblemClasses/MarginalSamplingProblem.h"
#include "MUQ/Inference/ProblemClasses/MarginalInferenceProblem.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Inference;
using namespace muq::Utilities;

TEST(InferenceMCMCTest, PseudoMarginalSampling)
{
  RandomGeneratorTemporarySetSeed tempSeed(56335740);

  // dimension of the joint
  const unsigned int Njoint = 5;

  // we are interested in the first 2 dimensions
  const unsigned int Nmarginal = 2;

  // the dimension of the part we are marginalizing
  const unsigned int Nconditional = Njoint - Nmarginal;

  // the joint distribution (standard normal)
  auto joint = make_shared<GaussianDensity>(Eigen::Vector2i(Nmarginal, Nconditional));

  // a random variable and density pair to do the importance sampling
  auto pair = make_shared<GaussianPair>(Nconditional, Nmarginal * Eigen::VectorXi::Ones(1));

  boost::property_tree::ptree params;

  // sampling problem options
  params.put<int>("MarginalSamplingProblem.MarginalizedIndex", 1);
  params.put<int>("MarginalSamplingProblem.NumImportanceSamples", 1e2);
  params.put<int>("MarginalSamplingProblem.PseudoMarginal", true);

  // the sampling problem
  auto problem = make_shared<MarginalSamplingProblem>(joint, pair, params);

  // mcmc options
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Proposal", "AM");
  params.put("MCMC.Steps", 110000);
  params.put("MCMC.BurnIn", 10000);
  params.put("MCMC.MHProposal.PropSize", 0.5);
  params.put("MCMC.AM.AdaptSteps", 2);
  params.put("MCMC.AM.AdaptStart", 5);
  params.put("MCMC.AM.AdaptScale", 1.2);
  params.put("Verbose", 0);

  auto solver = MCMCBase::ConstructMCMC(problem, params);

  // sample the distribution
  const Eigen::VectorXd start = Eigen::VectorXd::Zero(Nmarginal);
  EmpRvPtr posterior          = solver->Sample(start);

  // a really bad estimator because we have so few importance samples and mcmc steps...
  Eigen::Vector2d precomputedMean(0.32053352227643145, 0.14898839221017524);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, Eigen::VectorXd::Zero(2),posterior->getMean(), 1e-2);
}

/*TEST(InferenceMCMCTest, PseudoMarginalInference)
 *  {
 *  RandomGeneratorTemporarySetSeed tempSeed(56335740);
 *
 *  // dimension of parameter space const unsigned int dimP = 6;
 *
 *  // create a random matrix for the predictive model const unsigned int dimQ = 2;
 *  const Eigen::MatrixXd A = Eigen::MatrixXd::Identity(dimQ, dimP);
 *  auto fq                 = make_shared<LinearModel>(A);
 *
 *  // create a random matrix for the forward model const unsigned int dimD  = 5;
 *  const Eigen::MatrixXd F  = Eigen::MatrixXd::Identity(dimD, dimP);
 *  const Eigen::MatrixXd Ft = F.transpose();
 *  auto fd                  = make_shared<LinearModel>(F);
 *
 *  // find the qoi parallel space (and its complement) Eigen::MatrixXd qoiParallel;
 *  Eigen::MatrixXd qoiParallelComplement;
 *
 *  fq->NullSpace(qoiParallel, qoiParallelComplement);
 *
 *  Eigen::MatrixXd V(dimP, dimP);
 *
 *  V.leftCols(dimP - dimQ) = qoiParallel;
 *  V.rightCols(dimQ)       = qoiParallelComplement;
 *
 *  // models, given coeff. of parameter space vector compute the vector auto nullSpace      =
 *   make_shared<LinearModel>(qoiParallel);
 *  auto nullComplement = make_shared<LinearModel>(qoiParallelComplement);
 *
 *  // The prior is standard normal auto prior                = make_shared<GaussianDensity>(dimQ);
 *  const Eigen::VectorXd mu  = Eigen::VectorXd::Zero(dimP);
 *  const Eigen::MatrixXd cov = Eigen::MatrixXd::Identity(dimP, dimP);
 *  auto conditionalPrior     = make_shared<GaussianDensity>(dimP - dimQ, dimQ * Eigen::VectorXi::Ones(1));
 *
 *  // compute the data and define the likelihood auto errorRV              =
 *  make_shared<GaussianRV>(Eigen::VectorXd::Zero(dimD), 1.0e-5);
 *  Eigen::VectorXd thetaTrue = Eigen::VectorXd::Random(dimP);
 *  Eigen::VectorXd data      = fd->Evaluate(thetaTrue) + errorRV->Sample();
 *  const double    dataCov   = 1.0e-2;
 *  auto likelihood           = make_shared<GaussianDensity>(data, dataCov);
 *
 *  // find the posterior mean (this is a linear, Gaussian case) const Eigen::MatrixXd postPrec =
 *  Eigen::MatrixXd::Identity(dimP, dimP) + Ft * F / dataCov;
 *  const Eigen::MatrixXd postCov  = postPrec.inverse();
 *  const Eigen::VectorXd postMean = postCov * Ft * data / dataCov;
 *
 *  // A sampling pair for the importance sampler auto qPair = make_shared<GaussianPair>(dimP - dimQ, mu, cov);
 *
 *  // create a mod graph ModGraph graph;
 *  graph.AddNode(make_shared<VectorPassthroughModel>(dimQ), "target");
 *  graph.AddNode(nullSpace, "Null Space");
 *  graph.AddNode(nullComplement, "Null Complement");
 *  graph.AddNode(make_shared<SumModel>(2, dimP), "Full Parameter");
 *  graph.AddNode(prior, "prior");
 *  graph.AddNode(fd, "forwardModel");
 *  graph.AddNode(likelihood, "likelihood");
 *  graph.AddNode(make_shared<DensityProduct>(2), "posterior");
 *
 *  graph.AddEdge("target", "Null Complement", 0);
 *  graph.AddEdge("Null Complement", "Full Parameter", 1);
 *  graph.AddEdge("Null Space", "Full Parameter", 0);
 *  graph.AddEdge("target", "prior", 0);
 *  graph.AddEdge("Full Parameter", "forwardModel", 0);
 *  graph.AddEdge("forwardModel", "likelihood", 0);
 *  graph.AddEdge("prior", "posterior", 0);
 *  graph.AddEdge("likelihood", "posterior", 1);
 *
 *  vector<string> inOrder(2);
 *  inOrder[0] = "Null Space";
 *  inOrder[1] = "target";
 *
 *  const unsigned int numIPSamps      = 10;
 *  const unsigned int marginalizedDim = 0;
 *  auto problem                       = make_shared<MarginalInferenceProblem>(graph, conditionalPrior, qPair,
 *numIPSamps, marginalizedDim, inOrder);
 *
 *  // create an MCMC instance boost::property_tree::ptree params;
 *  params.put("MCMC.Method", "SingleChainMCMC");
 *  params.put("MCMC.Kernel", "MHKernel");
 *  params.put("MCMC.Proposal", "AM");
 *  params.put("MCMC.Steps", 1000);
 *  params.put("MCMC.BurnIn", 10);
 *  params.put("MCMC.MHProposal.PropSize", 0.5);
 *  params.put("MCMC.AM.AdaptSteps", 2);
 *  params.put("MCMC.AM.AdaptStart", 5);
 *  params.put("MCMC.AM.AdaptScale", 1.2);
 *  params.put("Verbose", 0);
 *
 *  auto solver = MCMCBase::ConstructMCMC(problem, params);
 *
 *  // sample the distribution const Eigen::VectorXd start = qoiParallelComplement.transpose() * postMean;
 *  EmpRvPtr posterior          = solver->Sample(start);
 *
 *  Eigen::VectorXd recordedMean(2);
 *  recordedMean << 0.72401259031823528, -0.10360298454587806;
 *
 *  EXPECT_PRED_FORMAT3(MatrixApproxEqual, posterior->getMean(), recordedMean, MCMCTOL);
 *
 *  Eigen::MatrixXd expectedCov(2, 2);
 *
 *  expectedCov << 5.17175e-06, 4.24566e-06, 4.24566e-06, 0.00108718;
 *
 *  EXPECT_PRED_FORMAT3(MatrixApproxEqual, posterior->getCov(), expectedCov, MCMCTOL);
 *  }*/


import unittest
import numpy as np # math stuff in python
import numpy.linalg as la
import random
import math
import libmuqModelling

# test the model implementation
class GaussianTest(unittest.TestCase):
    def testIsotropicDensity(self):
        N = 5

        dens = libmuqModelling.GaussianDensity(N)

        ins = [[random.random() for _ in range(0, N)]]

        self.assertAlmostEqual(dens.Evaluate(ins), -0.5 * (N*math.log(2*math.pi) + np.dot(np.array(ins[0]), np.array(ins[0]))), 8)

    def testScaledIsotropicDensity(self):
        N   = 5
        cov = 0.15

        dens = libmuqModelling.GaussianDensity(N, cov)

        ins = [[random.random() for _ in range(0, N)]]

        self.assertAlmostEqual(dens.Evaluate(ins), -0.5 *(N*math.log(2*math.pi) + N * np.log(cov) + np.dot(np.array(ins[0])/cov, np.array(ins[0]))), 8)

    def testScaledIsotropicDensity_NonzeroMean(self):
        N   = 5
        mu  = [random.random() for _ in range(0, N)]
        cov = 0.15

        dens = libmuqModelling.GaussianDensity(mu, cov)

        ins = [[random.random() for _ in range(0, N)]]

        delta = np.array(ins[0]) - np.array(mu)

        self.assertAlmostEqual(dens.Evaluate(ins), -0.5 *(N*math.log(2*math.pi) + N * np.log(cov) + np.dot(delta/cov, delta)), 8)

    def testDiagCov(self):
        random.seed(4)

        N   = 5
        mu  = [random.random() for _ in range(0, N)]
        cov = [random.random() for _ in range(0, N)]
        npCov = np.array(cov)
        covDet = np.prod(npCov)
        dens = libmuqModelling.GaussianDensity(mu, cov)

        ins = [[random.random() for _ in range(0, N)]]

        delta = np.array(ins[0]) - np.array(mu)

        self.assertAlmostEqual(dens.Evaluate(ins)[0], -0.5*(N*math.log(2*math.pi) + np.log(covDet) + np.dot(delta/npCov,delta)), 8)

    def testDiagCovSample(self):
        random.seed(12345)

        N   = 2
        mu  = [random.random() for _ in range(0, N)]
        cov = [random.random() for _ in range(0, N)]

        rv = libmuqModelling.GaussianRV(mu, cov)

        muApprox = np.sum(np.array(rv.Sample(int(1e4))), axis=1) / 1e4

        muPrecomputed = [0.41661987254534116, 0.010169169457068361]

        for i in range(N):
            self.assertEqual(mu[i], muPrecomputed[i])

    def testDiagCov_SecondConstructor(self):
        random.seed(4)

        N   = 5
        mu  = [random.random() for _ in range(0, N)]
        cov = [random.random() for _ in range(0, N)]
        npCov = np.array(cov)
        covDet = np.prod(npCov)

        dens = libmuqModelling.GaussianDensity(mu, cov, libmuqModelling.GaussianSpecification.DiagonalCovariance)

        ins = [[random.random() for _ in range(0, N)]]

        delta = np.array(ins[0]) - np.array(mu)

        self.assertAlmostEqual(dens.Evaluate(ins)[0], -0.5*(N*math.log(2*math.pi) + np.log(covDet) + np.dot(delta/npCov,delta)), 8)

    def testDiagPrec(self):
        random.seed(13)

        N   = 5
        mu  = [random.random() for _ in range(0, N)]
        prec = [random.random() for _ in range(0, N)]
        npPrec = np.array(prec)
        precDet = np.prod(npPrec)

        dens = libmuqModelling.GaussianDensity(mu, prec, libmuqModelling.GaussianSpecification.DiagonalPrecision)

        ins = [[random.random() for _ in range(0, N)]]
        delta = np.array(ins[0]) - np.array(mu)
        self.assertAlmostEqual(dens.Evaluate(ins)[0], -0.5*(N*math.log(2*math.pi) - np.log(precDet) + np.dot(delta*npPrec,delta)) ,8)

    def testFullCov(self):
        random.seed(18)

        N   = 5
        mu  = [random.random() for _ in range(0, N)]
        L   = np.array([[1.0e-1*i for i in range(N)] for j in range(N)])
        cov = (L*L.transpose()).tolist()
        for i in range(N):
            cov[i][i] = cov[i][i] + 1.0

        npCov = np.array(cov)
        covDet = la.det(npCov)

        dens = libmuqModelling.GaussianDensity(mu, cov, libmuqModelling.GaussianSpecification.CovarianceMatrix)

        ins = [[random.random() for _ in range(0, N)]]

        delta = np.array(ins[0]) - np.array(mu)

        self.assertAlmostEqual(dens.Evaluate(ins)[0], -0.5*(N*math.log(2*math.pi) + np.log(covDet) + np.dot(la.solve(npCov,delta),delta)), 8)

    def testFullPrec(self):
        random.seed(27)

        N    = 5
        mu   = [random.random() for _ in range(0, N)]
        L    = np.array([[1.0e-1*i for i in range(N)] for j in range(N)])
        prec = (L*L.transpose()).tolist()
        for i in range(N):
            prec[i][i] = prec[i][i] + 1.0

        dens = libmuqModelling.GaussianDensity(mu, prec, libmuqModelling.GaussianSpecification.PrecisionMatrix)

        ins = [[random.random() for _ in range(0, N)]]

        delta = np.array(ins[0]) - np.array(mu)
        precDet = la.det(prec)

        self.assertEqual(dens.Evaluate(ins)[0], -0.5*(N*math.log(2*math.pi) - np.log(precDet) + np.dot(np.dot(prec,delta),delta)),8)

    def testConditionalCov(self):
        random.seed(18)

        N     = 5
        Ncond = 2
        mu    = [random.random() for _ in range(0, N)]
        L     = np.array([[1.0e-1*i for i in range(N)] for j in range(N)])
        cov   = (L*L.transpose()).tolist()
        for i in range(N):
            cov[i][i] = cov[i][i] + 1.0

        dens = libmuqModelling.GaussianDensity(Ncond, mu, cov, libmuqModelling.GaussianSpecification.CovarianceMatrix)

        theta = [random.random() for _ in range(0, N)]

        ins = [theta[0:Ncond], theta[Ncond:N]]
        
        aMu = np.array(mu[0:Ncond])
        bMu = np.array(mu[Ncond:N])
        x = np.array(theta[0:Ncond])
        y = np.array(theta[Ncond:N])

        allCov = np.array(cov)
        A = allCov[0:Ncond,0:Ncond]
        B = allCov[Ncond:N,Ncond:N]
        C = allCov[0:Ncond,Ncond:N]
        
        condCov = A - np.dot(C,la.solve(B,np.transpose(C)))
        condMu = aMu + np.dot(C,la.solve(B,y-bMu))
        delta = x-condMu
        (temp,covLogDet) = la.slogdet(condCov)
        self.assertAlmostEqual(dens.Evaluate(ins)[0], -0.5*(Ncond*math.log(2*math.pi) + Ncond * covLogDet + np.dot(la.solve(condCov,delta),delta)), 1)

    def testConditionalPrec(self):
        random.seed(15)

        N     = 5
        Ncond = 2
        mu    = [random.random() for _ in range(0, N)]
        L     = np.array([[1.0e-1*i for i in range(N)] for j in range(N)])
        prec  = (L*L.transpose()).tolist()
        for i in range(N):
            prec[i][i] = prec[i][i] + 1.0

        dens = libmuqModelling.GaussianDensity(Ncond, mu, prec, libmuqModelling.GaussianSpecification.PrecisionMatrix)

        theta = [random.random() for _ in range(0, N)]

        ins = [theta[0:Ncond], theta[Ncond:N]]

        aMu = np.array(mu[0:Ncond])
        bMu = np.array(mu[Ncond:N])
        x = np.array(theta[0:Ncond])
        y = np.array(theta[Ncond:N])

        allCov = la.inv(np.array(prec))
        A = allCov[0:Ncond,0:Ncond]
        B = allCov[Ncond:N,Ncond:N]
        C = allCov[0:Ncond,Ncond:N]

        condCov = A - np.dot(C,la.solve(B,np.transpose(C)))
        condMu = aMu + np.dot(C,la.solve(B,y-bMu))
        delta = x-condMu
        (temp,covLogDet) = la.slogdet(condCov)

        self.assertAlmostEqual(dens.Evaluate(ins)[0], -0.5*(Ncond*math.log(2*math.pi) + Ncond * covLogDet + np.dot(la.solve(condCov,delta),delta)),1)
                         
    def testConditionalCovSample(self):
        random.seed(100)

        N     = 5
        Ncond = 2
        mu    = [random.random() for _ in range(0, N)]
        L     = np.array([[1.0e-1*i for i in range(N)] for j in range(N)])
        cov   = (L*L.transpose()).tolist()
        for i in range(N):
            cov[i][i] = cov[i][i] + 1.0

        rv = libmuqModelling.GaussianRV(Ncond, mu, cov, libmuqModelling.GaussianSpecification.CovarianceMatrix)

        theta = [random.random() for _ in range(0, N)]

        ins = [theta[Ncond:N]]

        muApprox = np.sum(np.array(rv.Sample(ins, int(5e4))), axis=1) / 5e4

        muPrecomputed = [0.1456692551041303, 0.45492700451402135]

        for i in range(Ncond):
            self.assertEqual(mu[i], muPrecomputed[i])

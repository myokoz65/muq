
# define the source files in this directory that we want to include in the
#   generic interface library
SET(interfaceSOURCES

     AnalyticFunctions/ComponentwiseModel.cpp
     CachedModPiece.cpp
     ConcatenateModel.cpp
     DaeModPiece.cpp
     Density.cpp
     DensityProduct.cpp
     EmpiricalRandVar.cpp
     GammaDensityRV.cpp
     IdentityObserver.cpp
     LinearModel.cpp 
     #LinOpMvNormSpecification.cpp
     ModGraph.cpp
     ModGraphPiece.cpp
     ModPiece.cpp
     ModPieceTemplates.cpp
     MultiplicationModPiece.cpp
     OdeModPiece.cpp
     RandVar.cpp
     RepeatedModel.cpp
     Slice2d.cpp
     SliceModel.cpp
     SolverModPiece.cpp
     SumModel.cpp
     UniformDensity.cpp
     UniformRandVar.cpp
     UniformSpecification.cpp
     Specification.cpp
     GaussianSpecification.cpp
     GaussianDensity.cpp
     GaussianRV.cpp
     RosenbrockSpecification.cpp
     RosenbrockRV.cpp
     RosenbrockDensity.cpp
     PointCache.cpp

#     ModelOptConstraint.cpp
#     MeshObserver.cpp
	    
ModGraphOperators.cpp
DifferenceModel.cpp
ProductModel.cpp
  )

if(MUQ_Sacado)
    set(interfaceSOURCES
        ${interfaceSOURCES}
        SacadoModPiece.cpp
        )
endif(MUQ_Sacado)

if(MUQ_USE_PYTHON)
    set(interfaceSOURCES
        ${interfaceSOURCES}
	
	../python/CachedModPiecePython.cpp 
	../python/PointCachePython.cpp 
        ../python/ModellingPythonLibrary.cpp  
	../python/ModGraphPiecePython.cpp 
	../python/DensityPython.cpp     
        ../python/ModGraphPython.cpp         
	../python/RandVarPython.cpp
	../python/EmpiricalRandVarPython.cpp   
        ../python/ModPiecePython.cpp    
	../python/UniformPython.cpp
	../python/LinearModelPython.cpp           
	../python/ModPieceTemplatesPython.cpp
	../python/SumModelPython.cpp
	../python/SliceModelPython.cpp
	../python/MultiplicationModPiecePython.cpp   
	../python/ModGraphOperationsPython.cpp
  ../python/OdeModPiecePython.cpp
	../python/GaussianDensityPython.cpp   
	../python/GaussianRVPython.cpp   
	../python/GaussianPairPython.cpp   
	../python/RosenbrockRVPython.cpp
	../python/RosenbrockDensityPython.cpp
	../python/RosenbrockPairPython.cpp   
	../python/ConcatenateModelPython.cpp   
	../python/ModPieceDensityPython.cpp   
	../python/ModParameterPython.cpp
	)
endif(MUQ_USE_PYTHON)

#add in the parallel stuff if we have mpi
if(MUQ_USE_OPENMPI)
  set(interfaceSOURCES ${interfaceSOURCES} 
  ParallelCachedModPiece.cpp)
endif(MUQ_USE_OPENMPI)



##########################################
#  Objects to build


# Define the library
ADD_LIBRARY(muqModelling ${interfaceSOURCES})
TARGET_LINK_LIBRARIES(muqModelling muqUtilities ${MUQ_LINK_LIBS})

if(APPLE AND MUQ_USE_PYTHON)
    set_property(TARGET muqModelling PROPERTY PREFIX "lib")
    set_property(TARGET muqModelling PROPERTY OUTPUT_NAME "muqModelling.so")
    set_property(TARGET muqModelling PROPERTY SUFFIX "")
    set_property(TARGET muqModelling PROPERTY SOVERSION "32.1.2.0")
endif(APPLE AND MUQ_USE_PYTHON)

install(TARGETS muqModelling
    EXPORT MUQDepends
    LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
    ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib")

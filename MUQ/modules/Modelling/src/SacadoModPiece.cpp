#include "MUQ/Modelling/SacadoModPiece.h"

#include "Sacado_tradvec.hpp"
#include "Sacado_Fad_DVFad.hpp"
#include "Sacado_Fad_SimpleFad.hpp"
#include "Sacado_CacheFad_DFad.hpp"
#include "Sacado_CacheFad_SFad.hpp"
#include "Sacado_CacheFad_SLFad.hpp"


using namespace std;
using namespace muq::Modelling;


/** Evaluate the gradient of this model using Sacado automatic differentiation. */
Eigen::VectorXd SacadoModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & sensitivity,
                                             int const                           inputDimWrt)
{
  // convert to reverse RadType
  std::vector < Eigen::Matrix < RadType, Eigen::Dynamic, 1 >> allRADx(input.size());

  for (unsigned int k = 0; k < input.size(); ++k) {
    allRADx[k].resize(input[k].size());
    for (unsigned int i = 0; i < input[k].size(); ++i) {
      allRADx[k](i) = input[k](i);
    }
  }

  // allocate memory for the result
  Eigen::Matrix<RadType, Eigen::Dynamic, 1> RADresult(sensitivity.size());

  // create a pointer to the results
  RadType *v[sensitivity.size()];
  for (unsigned int i = 0; i < sensitivity.size(); ++i) {
    v[i] = &RADresult(i);
  }

  // a pointer to the pointer of results (this way of inputing things makes more sense if we have more than one)
  RadType **V[1]  = { v };
  size_t    np[1] = { (size_t)sensitivity.size() };

  // a pointer to the sensitivity vector (again makes more sense if we have more than one)
  double w[sensitivity.size()];
  for (unsigned int i = 0; i < sensitivity.size(); ++i) {
    w[i] = sensitivity(i);
  }
  double *W[1] = { w };

  // run the forward model
  RADresult = EvaluateImpl(allRADx);

  // run reverse AD
  RadType::Weighted_GradcompVec(1, np, V, W);

  // extract the gradient
  Eigen::VectorXd grad(input[inputDimWrt].size());
  for (unsigned int i = 0; i < input[inputDimWrt].size(); ++i) {
    grad(i) = allRADx[inputDimWrt](i).adj(0);
  }

  return grad;
}

/** Compute the jacobian of this model using Sacado automatic differentiation. */
Eigen::MatrixXd SacadoModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  return (inputSizes[inputDimWrt] > outputSize) ? Jacobian_reverse(input, inputDimWrt) : Jacobian_forward(input,
                                                                                                          inputDimWrt);
}

/** Evaluate the action of the Jacobian matrix on a vector using Sacado AD to compute the Jacobian. */
Eigen::VectorXd SacadoModPiece::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                   Eigen::VectorXd const             & target,
                                                   int const                           inputDimWrt)
{
  assert(input[inputDimWrt].size() == target.size());

  // convert to forward FadType applied to one perturbation vector
  std::vector < Eigen::Matrix < FadType, Eigen::Dynamic, 1 >> allFADx(input.size());
  for (unsigned int k = 0; k < input.size(); ++k) {
    allFADx[k].resize(input[k].size());

    for (unsigned int i = 0; i < input[k].size(); ++i) {
      allFADx[k](i) = FadType(1, input[k](i));
    }
  }
  for (unsigned int i = 0; i < input[inputDimWrt].size(); ++i) {
    allFADx[inputDimWrt](i).fastAccessDx(0) = target(i);
  }

  // compute the forward model
  Eigen::Matrix<FadType, Eigen::Dynamic, 1> FADresult = EvaluateImpl(allFADx);

  // extract the desired product
  Eigen::VectorXd Jy(FADresult.size());
  for (unsigned int i = 0; i < FADresult.size(); ++i) {
    Jy(i) = FADresult(i).dx(0);
  }

  return Jy;
}

Eigen::MatrixXd SacadoModPiece::Jacobian_forward(const std::vector<Eigen::VectorXd>& x, int const inputDimWrt)
{
  // convert to forward sacado type
  std::vector < Eigen::Matrix < FadType, Eigen::Dynamic, 1 >> allFADx(x.size());

  for (int k = 0; k < int(x.size()); ++k) {
    allFADx[k].resize(x[k].size());

    if (k == inputDimWrt) {
      for (int i = 0; i < inputSizes[inputDimWrt]; ++i) {
        allFADx[k](i) = FadType(inputSizes[inputDimWrt], i, x[k](i));
      }
    } else {
      for (unsigned int i = 0; i < x[k].size(); ++i) {
        allFADx[k](i) = FadType(0, x[k](i));
      }
    }
  }

  Eigen::Matrix<FadType, Eigen::Dynamic, 1> FADresult = EvaluateImpl(allFADx);

  Eigen::MatrixXd J(outputSize, inputSizes[inputDimWrt]);
  for (int i = 0; i < outputSize; ++i) {
    for (int j = 0; j < inputSizes[inputDimWrt]; ++j) {
      J(i, j) = FADresult(i).dx(j);
    }
  }

  return J;
}

Eigen::MatrixXd SacadoModPiece::Jacobian_reverse(const std::vector<Eigen::VectorXd>& x, int const inputDimWrt)
{
  // convert to reverse RadType
  std::vector < Eigen::Matrix < RadType, Eigen::Dynamic, 1 >> allRADx(x.size());

  for (unsigned int k = 0; k < x.size(); ++k) {
    allRADx[k].resize(x[k].size());
    for (unsigned int i = 0; i < x[k].size(); ++i) {
      allRADx[k](i) = x[k](i);
    }
  }

  // run the forward model
  Eigen::Matrix<RadType, Eigen::Dynamic, 1> RADresult = EvaluateImpl(allRADx);

  // compute the Jacobian
  Eigen::MatrixXd J(outputSize, inputSizes[inputDimWrt]);
  for (int i = 0; i < outputSize; ++i) {
    // run reverse AD
    RadType::Outvar_Gradcomp(RADresult(i));

    for (int j = 0; j < inputSizes[inputDimWrt]; ++j) {
      J(i, j) = allRADx[inputDimWrt](j).adj();
    }
  }

  return J;
}

/** Evaluate the hessian with Sacado */
Eigen::MatrixXd SacadoModPiece::HessianImpl(std::vector<Eigen::VectorXd> const& x,
                                            Eigen::VectorXd const             & sensitivity,
                                            int const                           inputDimWrt)
{
  return (inputSizes[inputDimWrt] > outputSize) ? Hessian_forward_reverse(x, sensitivity,
                                                                          inputDimWrt) : Hessian_forward_forward(x,
                                                                                                                 sensitivity,
                                                                                                                 inputDimWrt);
}

/** first derivative in forward mode, second derivative in forward mode */
Eigen::MatrixXd SacadoModPiece::Hessian_forward_forward(const std::vector<Eigen::VectorXd>& x,
                                                        Eigen::VectorXd const             & sensitivity,
                                                        int const                           inputDimWrt)
{
  // convert to forward sacado type
  std::vector < Eigen::Matrix < FadFadType, Eigen::Dynamic, 1 >> allFADFADx(x.size());

  for (int k = 0; k < int(x.size()); ++k) {
    allFADFADx[k].resize(x[k].size());

    if (k == inputDimWrt) {
      for (int i = 0; i < inputSizes[inputDimWrt]; ++i) {
        const FadType in = FadType(inputSizes[inputDimWrt], i, x[k](i));
        allFADFADx[k](i) = FadFadType(inputSizes[inputDimWrt], i, in);
      }
    } else {
      for (unsigned int i = 0; i < x[k].size(); ++i) {
        const FadType in = FadType(0, x[k](i));
        allFADFADx[k](i) = FadFadType(0, in);
      }
    }
  }

  const Eigen::Matrix<FadFadType, Eigen::Dynamic, 1> FADFADresult = EvaluateImpl(allFADFADx);

  Eigen::MatrixXd Hessian = Eigen::MatrixXd::Zero(inputSizes[inputDimWrt], inputSizes[inputDimWrt]);
  for (int s = 0; s < outputSize; ++s) {
    for (int j = 0; j < inputSizes[inputDimWrt]; ++j) {
      for (int i = 0; i < inputSizes[inputDimWrt]; ++i) {
        Hessian(i, j) += sensitivity(s) * FADFADresult(s).dx(i).dx(j);
      }
    }
  }

  return Hessian;
}

/** first derivative in reverse mode, second derivative in forward mode */
Eigen::MatrixXd SacadoModPiece::Hessian_forward_reverse(const std::vector<Eigen::VectorXd>& x,
                                                        Eigen::VectorXd const             & sensitivity,
                                                        int const                           inputDimWrt)
{
  // convert to forward-reverse sacado type
  std::vector < Eigen::Matrix < FadRadType, Eigen::Dynamic, 1 >> allFADRADx(x.size());

  for (int k = 0; k < int(x.size()); ++k) {
    allFADRADx[k].resize(x[k].size());

    if (k == inputDimWrt) {
      for (int i = 0; i < inputSizes[inputDimWrt]; ++i) {
        const FadType in = FadType(inputSizes[inputDimWrt], i, x[k](i));
        allFADRADx[k](i) = in;
      }
    } else {
      for (unsigned int i = 0; i < x[k].size(); ++i) {
        const FadType in = FadType(0, x[k](i));
        allFADRADx[k](i) = in;
      }
    }
  }

  Eigen::Matrix<FadRadType, Eigen::Dynamic, 1> FADRADresult = EvaluateImpl(allFADRADx);

  Eigen::MatrixXd Hessian = Eigen::MatrixXd::Zero(inputSizes[inputDimWrt], inputSizes[inputDimWrt]);
  for (int s = 0; s < outputSize; ++s) {
    FadRadType::Outvar_Gradcomp(FADRADresult(s));
    for (int i = 0; i < inputSizes[inputDimWrt]; ++i) {
      for (int j = 0; j < inputSizes[inputDimWrt]; ++j) {
        Hessian(i, j) += sensitivity(s) * allFADRADx[inputDimWrt](i).adj().dx(j);
      }
    }
  }

  return Hessian;
}


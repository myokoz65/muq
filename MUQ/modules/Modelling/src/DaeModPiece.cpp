

#include "MUQ/Modelling/DaeModPiece.h"

#include <boost/property_tree/ptree.hpp>

#include <idas/idas.h>           /* main integrator header file */
#include <idas/idas_spgmr.h>     /* prototypes & constants for CVSPGMR solver */
#include <idas/idas_spbcgs.h>    /* prototypes & constants for CVSPBCG solver */
#include <idas/idas_sptfqmr.h>   /* prototypes & constants for SPTFQMR solver */
#include <idas/idas_dense.h>

#include <nvector/nvector_serial.h>  /* serial N_Vector types, fct. and macros */
#include <sundials/sundials_dense.h> /* use generic DENSE solver in preconditioning */
#include <sundials/sundials_types.h> /* definition of realtype */
#include <sundials/sundials_math.h>  /* contains the macros ABS, SQR, and EXP */

#include "MUQ/Modelling/IdentityObserver.h"


using namespace muq::Modelling;
using namespace std;

struct RhsData {
  std::shared_ptr<ModPiece>    f, g;
  std::vector<Eigen::VectorXd> inputs;
  int                          stateSize;
  int                          inputDimWrt;
  int                          paramSize;
};

static void sundialsErrorFunc(int error_code, const char *module, const char *function, char *msg, void *eh_data) {}

/* Functions Called by the IDAS Solver */
static int  idasFunc(realtype time,
                     N_Vector state,
					 N_Vector deriv,
					 N_Vector resid,
					 void *user_data)
{
  RhsData *fgdata = (RhsData *)user_data;

  Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvDerivMap(NV_DATA_S(deriv), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvResidMap(NV_DATA_S(resid), fgdata->stateSize);

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = nvStateMap;
  fgdata->inputs.at(2) = nvDerivMap;

  nvResidMap = fgdata->f->Evaluate(fgdata->inputs);

  return 0;
}

static int idasJac(long int N,
                   realtype time,
				   realtype alpha,
                   N_Vector state,
				   N_Vector deriv,
                   N_Vector resid,
                   DlsMat   jac,
                   void    *user_data,
                   N_Vector tmp1,
                   N_Vector tmp2,
                   N_Vector tmp3)
{
  RhsData *fgdata = (RhsData *)user_data;

      assert(N == fgdata->stateSize);

  Eigen::Map<Eigen::VectorXd> stateMap(NV_DATA_S(state), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> derivMap(NV_DATA_S(deriv), fgdata->stateSize);
  
      assert(jac->M == fgdata->stateSize);

  Eigen::Map < Eigen::MatrixXd, 0, Eigen::OuterStride < Eigen::Dynamic >> jacMap(jac->data,
                                                                                 jac->M,
                                                                                 jac->N,
                                                                                 Eigen::OuterStride<Eigen::Dynamic>(jac
                                                                                                                    ->
                                                                                                                    ldim));

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = stateMap;
  fgdata->inputs.at(2) = derivMap;

  fgdata->f->Evaluate(fgdata->inputs);
  jacMap = fgdata->f->Jacobian(fgdata->inputs, 1) + alpha*fgdata->f->Jacobian(fgdata->inputs, 2);

  return 0;
}

static int idasJacApply(realtype time,
                        N_Vector state,
                        N_Vector deriv,
						N_Vector resid,
                        N_Vector v,
                        N_Vector Jv,
						realtype alpha,
                        void    *user_data,
                        N_Vector tmp1,
						N_Vector tmp2)
{
  RhsData *fgdata = (RhsData *)user_data;

  Eigen::Map<Eigen::VectorXd> vMap(NV_DATA_S(v), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> JvMap(NV_DATA_S(Jv), fgdata->stateSize);
  
  Eigen::Map<Eigen::VectorXd> stateMap(NV_DATA_S(state), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> derivMap(NV_DATA_S(deriv), fgdata->stateSize);

  //Eigen::Map<Eigen::VectorXd> fyMap(NV_DATA_S(deriv),fgdata->stateSize);

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = stateMap;
  fgdata->inputs.at(2) = derivMap;

  fgdata->f->Evaluate(fgdata->inputs);
  JvMap = fgdata->f->JacobianAction(fgdata->inputs, vMap, 1) + alpha*fgdata->f->JacobianAction(fgdata->inputs, vMap, 2);

  return 0;
}


static void check_sundials_flag(void *flagvalue, string funcname, int opt)
{
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if ((opt == 0) && (flagvalue == NULL)) {
    std::cerr << "\nSUNDIALS_ERROR: " << funcname << " failed - returned NULL pointer\n\n";
      assert(flagvalue);
  }
  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *)flagvalue;
    if (*errflag < 0) {
      std::cerr << "\nSUNDIALS_ERROR: " << funcname << " failed with flag = " << *errflag << "\n\n";
      assert(flagvalue >= 0);
    }
  }
  /* Check if function returned NULL pointer - no memory allocated */
  else if ((opt == 2) && (flagvalue == NULL)) {
    std::cerr << "\nMEMORY_ERROR: " << funcname << " failed - returned NULL pointer\n\n";
      assert(flagvalue);
  }
}

DaeModPiece::DaeModPiece(std::shared_ptr<ModPiece>    fIn,
                         std::shared_ptr<ModPiece>    gIn,
                         Eigen::VectorXd const      & evalTimes,
                         boost::property_tree::ptree& properties) : ModPiece(fIn->inputSizes.tail(fIn->inputSizes.size() - 1),
                                                                             gIn->outputSize * evalTimes.rows(), false, true, false, false,
                                                                             gIn->isRandom),
                                                                    f(fIn), g(gIn), obsTimes(evalTimes),
                                                                    linSolver(properties.get("IDAS.LinearSolver", "Dense")),
                                                                    abstol(properties.get("IDAS.ATOL", 1e-3)),
                                                                    reltol(properties.get("IDAS.RTOL",1e-5)),
                                                                    intMethod(properties.get("IDAS.Method","BDF")),
                                                                    solveMethod(properties.get("IDAS.SolveMethod", "Newton")),
                                                                    maxStepSize(properties.get("IDAS.MaxStepSize",1e20)),
                                                                    checkPtGap(properties.get("IDAS.CheckPtGap",50))
{
  assert(!fIn->isRandom);
  
  // check sizes of f and g
  assert(fIn->outputSize == fIn->inputSizes(1));
  assert(fIn->inputSizes(0) == 1);
  assert(gIn->inputSizes(0) == 1);
  assert(gIn->inputSizes(1) == fIn->inputSizes(1));
  assert(gIn->inputSizes(2) == fIn->inputSizes(2));
  assert(gIn->inputSizes.size() == fIn->inputSizes.size());
	  
  for (int inDim = 2; inDim < inputSizes.size(); ++inDim)
      assert(gIn->inputSizes(inDim) == fIn->inputSizes(inDim));
  
}

DaeModPiece::DaeModPiece(std::shared_ptr<ModPiece> fIn, std::shared_ptr<ModPiece> gIn,
                         Eigen::VectorXd const& evalTimes) : ModPiece(fIn->inputSizes.tail(fIn->inputSizes.size() - 1),
                                                                      gIn->outputSize * evalTimes.rows(), false, true, false, false,
                                                                      gIn->isRandom),
                                                             f(fIn), g(gIn),
                                                             obsTimes(evalTimes),
                                                             linSolver("Dense"),
                                                             abstol(1e-3),
                                                             reltol(1e-5),
                                                             intMethod("BDF"),
                                                             solveMethod("Newton"),
                                                             maxStepSize(1e20),
                                                             checkPtGap(50)
{
  assert(!fIn->isRandom);
  
  // check sizes of f and g
  assert(fIn->outputSize == fIn->inputSizes(1));
  assert(fIn->inputSizes(0) == 1);
  assert(gIn->inputSizes(0) == 1);
  assert(gIn->inputSizes(1) == fIn->inputSizes(1));

  assert(gIn->inputSizes.size() == fIn->inputSizes.size());
  for (int inDim = 2; inDim < inputSizes.size(); ++inDim)
      assert(gIn->inputSizes(inDim) == fIn->inputSizes(inDim));
  
}

DaeModPiece::DaeModPiece(std::shared_ptr<ModPiece>    fIn,
                         Eigen::VectorXd const      & evalTimes,
                         boost::property_tree::ptree& properties) : DaeModPiece(fIn,
                                                                                make_shared<IdentityObserver>(fIn->inputSizes,1),
                                                                                evalTimes,
                                                                                properties)
{}

DaeModPiece::DaeModPiece(std::shared_ptr<ModPiece> fIn, Eigen::VectorXd const& evalTimes) : DaeModPiece(fIn,
                                                                                                        make_shared<IdentityObserver>(fIn->inputSizes,1),
                                                                                                        evalTimes)
{}


void DaeModPiece::RunIdasForward(std::vector<Eigen::VectorXd> const& inputs,
                                 bool                                runSens,
                                 int const                           inputDimWrt,
                                 Eigen::VectorXd                   & eval,
                                 Eigen::MatrixXd                   & jac) const
{
  // currently no sensitivity information is implemented
  assert(!runSens);
  
// if sensitivities are requested, make sure f has a jacobian function
// if (runSens) {
//     assert(f->hasDirectJacobian);
//   }

  N_Vector state, deriv;
  RhsData *fgdata;
  void    *ida_mem;
  int iout, flag;

  state     = NULL;
  deriv     = NULL;
  fgdata    = NULL;
  ida_mem = NULL;

  int stateSize = inputSizes(0);

  /* Allocate memory, and set problem data, initial values, tolerances */
  state = N_VNew_Serial(stateSize);
  check_sundials_flag((void *)state,  "N_VNew_Serial", 0);
  
  deriv = N_VNew_Serial(stateSize);
  check_sundials_flag((void *)deriv,  "N_VNew_Serial", 0);

  // resid = N_VNew_Serial(stateSize);
//   check_sundials_flag((void *)resid,  "N_VNew_Serial", 0);

  fgdata = new RhsData;
  check_sundials_flag((void *)fgdata, "AllocUserData", 2);

  // set the rhs and observation modpieces
  fgdata->stateSize = stateSize;
  fgdata->f         = f;
  fgdata->g         = g;
  fgdata->inputs.push_back(Eigen::VectorXd::Zero(1));
  fgdata->inputs.insert(fgdata->inputs.end(), inputs.begin(), inputs.end());

  // copy the initial state
  Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), stateSize);
  nvStateMap = inputs.at(0);

  Eigen::Map<Eigen::VectorXd> nvDerivMap(NV_DATA_S(deriv), stateSize);
  nvDerivMap = inputs.at(1);
  
  /* Call IDACreate to create the DAE solver memory and specify the
   * Backward Differentiation Formula and the use of a Newton iteration */
  ida_mem = IDACreate();
  check_sundials_flag((void *)ida_mem, "IDACreate", 0);

  /* Set the pointer to user-defined data */
  flag = IDASetUserData(ida_mem, fgdata);
  check_sundials_flag(&flag, "IDASetUserData",     1);

  /* Tell sundials what function to call if it runs into an error. */
  flag = IDASetErrHandlerFn(ida_mem, sundialsErrorFunc, fgdata);
  check_sundials_flag(&flag, "IDASetErrHandlerFn", 1);

  /* Call CVodeInit to initialize the integrator memory and specify the
   * user's right hand side function in u'=f(t,u), the inital time T0, and
   * the initial dependent variable vector u. */
  flag = IDAInit(ida_mem, idasFunc, 0.0, state, deriv);
  check_sundials_flag(&flag, "IDAInit", 1);

  /* Call CVodeSStolerances to specify the scalar relative tolerance
   * and scalar absolute tolerances */
  flag = IDASStolerances(ida_mem, reltol, abstol);
  check_sundials_flag(&flag, "IDASStolerances", 1);

  // the size of the parameter
  const int paramSize = inputSizes(inputDimWrt);

  if (!linSolver.compare("Dense")) {
    /* Call CVDense to specify the CVDENSE dense linear solver */
    flag = IDADense(ida_mem, stateSize);
    check_sundials_flag(&flag, "IDADense", 1);

    /* Set the Jacobian routine to Jac (user-supplied) */
    flag = IDADlsSetDenseJacFn(ida_mem, idasJac);
    check_sundials_flag(&flag, "IDADlsSetDenseJacFn", 1);
	
  } else {
	  
    if (!linSolver.compare("SPGMR")) {
      flag = IDASpgmr(ida_mem, 0);
      check_sundials_flag(&flag, "IDASpgmr",   1);
    } else if (!linSolver.compare("SPBCG")) {
      flag = IDASpbcg(ida_mem, 0);
      check_sundials_flag(&flag, "IDASpbcg",   1);
    } else if (!linSolver.compare("SPTFQMR")) {
      flag = IDASptfqmr(ida_mem, 0);
      check_sundials_flag(&flag, "IDASptfqmr", 1);
    } else {
      std::cerr << "\nInvalid IDAS linear solver type.  Options are Dense, SPGMR, SPBCG, or SPTFQMR\n\n";
      assert(false);
    }
	
	
    /* set the Jacobian-times-vector function */
    flag = IDASpilsSetJacTimesVecFn(ida_mem, idasJacApply);
    check_sundials_flag(&flag, "IDASpilsSetJacTimesVecFn", 1);
  }
  
  /* Set the preconditioner solve and setup functions */
  //flag = CVSpilsSetPreconditioner(cvode_mem, Precond, PSolve);
  //if(check_sundials_flag(&flag, "CVSpilsSetPreconditioner", 1)) return(1);

  /* In loop over output points, call IDA, print results, test for error */
  double tout = 0;
  eval = Eigen::VectorXd::Constant(outputSize, std::numeric_limits<double>::signaling_NaN());
  Eigen::Map<Eigen::MatrixXd> outMap(eval.data(), g->outputSize, obsTimes.size());

  // loop over all the observation times
  for (int i = 0; i < obsTimes.size(); ++i) {
    if (fabs(obsTimes(i) - tout) > 1e-14) {
      flag = IDASolve(ida_mem, obsTimes(i), &tout, state, deriv, IDA_NORMAL);

      // if there was an error, clean up the memory and return
      if (flag < 0) {
        N_VDestroy_Serial(state);
		N_VDestroy_Serial(deriv);
        delete fgdata;
        IDAFree(&ida_mem);
        return;
      }
    }

    // evaluate g and store the results
    fgdata->inputs.at(0) = obsTimes(i) * Eigen::VectorXd::Ones(1);
    fgdata->inputs.at(1) = Eigen::Map<Eigen::VectorXd>(NV_DATA_S(state), stateSize);
    fgdata->inputs.at(2) = Eigen::Map<Eigen::VectorXd>(NV_DATA_S(deriv), stateSize);

    outMap.col(i) = g->Evaluate(fgdata->inputs);
 
  }

  /* Free memory */
  N_VDestroy_Serial(state);
  N_VDestroy_Serial(deriv);

  delete fgdata;
  IDAFree(&ida_mem);
}


Eigen::VectorXd DaeModPiece::EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs)
{
  Eigen::VectorXd output;
  Eigen::MatrixXd jac;

  RunIdasForward(inputs, false, 0, output, jac);

  return output;
}

#include "MUQ/Modelling/AnalyticRandVar.h"

using namespace muq::utilities;
using namespace muq::Modelling;
using namespace std;


/** Generate a latin hypercube sample of size N from this distribution. */
Eigen::MatrixXd AnalyticRandVar::LhsSample(unsigned int N)
{
  // first, get samples of the random variable
  Eigen::MatrixXd samps(DimOut, N);    // the raw samples
  Eigen::MatrixXd LhsSamps(DimOut, N); // the latin hypercube samples

  for (unsigned int i = 0; i < N; ++i) {
    Eigen::VectorXd temp(DimOut);
    sample(temp);
    samps.col(i) = temp;
  }

  Eigen::VectorXd sMu  = samps.rowwise().sum() / N;
  Eigen::MatrixXd sCov = (samps.colwise() - sMu) * (samps.colwise() - sMu).transpose();
  sCov /= N;

  // sort each dimension of the samples and store the index
  std::vector<unsigned int> inds(N);
  for (int d = 0; d < DimOut; ++d) {
    // set the indices to the identity
    for (unsigned int i = 0; i < N; ++i) {
      inds[i] = i;
    }

    // sort the indices based on this dimension of the sample
    sort(inds.begin(), inds.end(), [&samps, &d](unsigned int i1, unsigned int i2) { return samps(d, i1) < samps(d,
                                                                                                                i2); });
    Eigen::VectorXd x(N);
    for (unsigned int i = 0; i < N; ++i) {
      x(inds[i]) = i + 0.5;
    }
    x /= N;

    // std::cout << x.transpose() << std::endl;

    // add a random perturbation to the sample and get the inverse RandomGenerator::GetUniform()
    for (unsigned int i = 0; i < N; ++i) {
      LhsSamps(d, i) = InverseCDF(d, x(i));
    }
  }

  return LhsSamps;
}



#include "MUQ/Modelling/SumModel.h"

using namespace std;
using namespace muq::Modelling;

SumModel::SumModel(int const numInputs, int const outputSize) :
  ModPiece(outputSize * Eigen::VectorXi::Ones(numInputs), outputSize, true, true, true, true, false)
{}

Eigen::VectorXd SumModel::EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs)
{
  Eigen::VectorXd result = Eigen::VectorXd::Zero(outputSize);

  for (unsigned int i = 0; i < inputSizes.size(); ++i) {
    result += inputs[i];
  }

  return result;
}

Eigen::MatrixXd SumModel::JacobianImpl(vector<Eigen::VectorXd> const&, int const)
{
  return Eigen::MatrixXd::Identity(outputSize, outputSize);
}

Eigen::VectorXd SumModel::JacobianActionImpl(vector<Eigen::VectorXd> const&,
                                             Eigen::VectorXd const& target,
                                             int const)
{
  return target;
}

Eigen::VectorXd SumModel::GradientImpl(vector<Eigen::VectorXd> const&,
                                       Eigen::VectorXd const& sens,
                                       int const)
{
  return sens;
}

Eigen::MatrixXd SumModel::HessianImpl(vector<Eigen::VectorXd> const&,
                                      Eigen::VectorXd const&,
                                      int const)
{
  return Eigen::MatrixXd::Zero(outputSize, outputSize);
}


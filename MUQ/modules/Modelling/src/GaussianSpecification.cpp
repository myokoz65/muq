#include "MUQ/Modelling/GaussianSpecification.h"

#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Modelling/LinearModel.h"
#include "MUQ/Modelling/SumModel.h"

using namespace std;
using namespace muq::Modelling;

GaussianSpecification::GaussianSpecification(shared_ptr<ModGraph> const& graph,
                                             SpecificationMode const   & mode,
                                             string const              & meanName,
                                             string const              & covName) :
  Specification(graph->outputSize(meanName) * Eigen::VectorXi::Ones(1), graph,
                MakeNameVector(meanName, covName)), mode(mode) {}

GaussianSpecification::GaussianSpecification(unsigned int const dimIn) :
  Specification(dimIn * Eigen::VectorXi::Ones(1), ScaledCovGraph(dimIn),
                MakeNameVector("mean", "cov")), mode(ScaledIdentity) {}

GaussianSpecification::GaussianSpecification(Eigen::VectorXi const& dimIn) :
  Specification(dimIn, ScaledCovGraph(dimIn.sum()),
                MakeNameVector("mean", "cov")), mode(ScaledIdentity) {}

GaussianSpecification::GaussianSpecification(unsigned int const dimIn, Eigen::VectorXi const& inputs) :
  Specification(dimIn * Eigen::VectorXi::Ones(1), ScaledCovGraph(dimIn),
                MakeNameVector("mean", "cov")), mode(ScaledIdentity) {}

GaussianSpecification::GaussianSpecification(unsigned int const dimIn, double const scaledCov) :
  Specification(dimIn * Eigen::VectorXi::Ones(1),
                ScaledCovGraph(dimIn, scaledCov), MakeNameVector("mean", "cov")), mode(ScaledIdentity) {}

GaussianSpecification::GaussianSpecification(Eigen::VectorXd const& mu, double const scaledCov) :
  Specification(mu.size() * Eigen::VectorXi::Ones(1),
                ScaledCovNonzeroMeanGraph(mu, scaledCov), MakeNameVector("mean", "cov")), mode(ScaledIdentity) {}

GaussianSpecification::GaussianSpecification(Eigen::VectorXd const  & mu,
                                             Eigen::VectorXd const  & diag,
                                             SpecificationMode const& mode) :
  Specification(mu.size() * Eigen::VectorXi::Ones(1),
                DiagonalGraph(mu, diag, mode),
                MakeNameVector("mean", (mode == DiagonalCovariance) ? "cov" : "prec")), mode(mode) {}

GaussianSpecification::GaussianSpecification(Eigen::VectorXi const  & dimIn,
                                             Eigen::VectorXd const  & mu,
                                             Eigen::VectorXd const  & diag,
                                             SpecificationMode const& mode) :
  Specification(dimIn,
                DiagonalGraph(mu, diag, mode),
                MakeNameVector("mean", (mode == DiagonalCovariance) ? "cov" : "prec")), mode(mode)
{
  assert(dimIn.sum() == mu.size());
}

GaussianSpecification::GaussianSpecification(Eigen::VectorXd const  & mu,
                                             Eigen::MatrixXd        & mat,
                                             SpecificationMode const& mode) :
  Specification(mu.size() * Eigen::VectorXi::Ones(1),
                FullGraph(mu, mat, mode),
                MakeNameVector("mean", (mode == CovarianceMatrix) ? "cov" : "prec")), mode(mode)
{
  // If there are no inputs we can precompute the Cholesky
  assert(paraMods[names[1]].first->inputSizes.size() == 0);

  ComputeCholesky();
}

GaussianSpecification::GaussianSpecification(Eigen::VectorXi const  & dimIn,
                                             Eigen::VectorXd const  & mu,
                                             Eigen::MatrixXd        & mat,
                                             SpecificationMode const& mode) :
  Specification(dimIn,
                FullGraph(mu, mat, mode),
                MakeNameVector("mean", (mode == CovarianceMatrix) ? "cov" : "prec")), mode(mode)
{
  assert(dimIn.sum() == mu.size());

  // If there are no inputs we can precompute the Cholesky
  assert(paraMods[names[1]].first->inputSizes.size() == 0);

  ComputeCholesky();
}

GaussianSpecification::GaussianSpecification(unsigned int const       dim,
                                             Eigen::VectorXd const  & mu,
                                             Eigen::MatrixXd        & mat,
                                             SpecificationMode const& mode) :
  Specification(dim * Eigen::VectorXi::Ones(1), ConditionalGraph(dim, mu, mat, mode),
                MakeNameVector("mean", (mode == CovarianceMatrix) ? "cov" : "prec")), mode(mode)
{
  // If there are no inputs we can precompute the Cholesky
  assert(paraMods[names[1]].first->inputSizes.size() == 0);

  ComputeCholesky();
}

vector<string> GaussianSpecification::MakeNameVector(string const& meanName, string const& covName)
{
  vector<string> names(2);
  names[0] = meanName;
  names[1] = covName;

  return names;
}

shared_ptr<ModGraph> GaussianSpecification::DiagonalGraph(Eigen::VectorXd const  & mu,
                                                          Eigen::VectorXd const  & diag,
                                                          SpecificationMode const& mode)
{
  auto graph = make_shared<ModGraph>();

  // add node for mean
  graph->AddNode(make_shared<ModParameter>(mu), "mean");

  // add node for covariance
  graph->AddNode(make_shared<ModParameter>(diag), (mode == DiagonalCovariance) ? "cov" : "prec");

  return graph;
}

shared_ptr<ModGraph> GaussianSpecification::ScaledCovGraph(unsigned int const dimIn, double const scaledCov)
{
  auto graph = make_shared<ModGraph>();

  // add node for mean
  graph->AddNode(make_shared<ModParameter>(Eigen::VectorXd::Zero(dimIn)), "mean");

  // add node for covariance
  graph->AddNode(make_shared<ModParameter>(scaledCov * Eigen::VectorXd::Ones(1)), "cov");

  return graph;
}

shared_ptr<ModGraph> GaussianSpecification::ScaledCovNonzeroMeanGraph(Eigen::VectorXd const mu, double const scaledCov)
{
  auto graph = make_shared<ModGraph>();

  // add node for mean
  graph->AddNode(make_shared<ModParameter>(mu), "mean");

  // add node for covariance
  graph->AddNode(make_shared<ModParameter>(scaledCov * Eigen::VectorXd::Ones(1)), "cov");

  return graph;
}

Eigen::VectorXd GaussianSpecification::Mean(std::vector<Eigen::VectorXd> const& inputs)
{
  vector<unsigned int> indices = paraMods[names[0]].second;

  vector<Eigen::VectorXd> ins(indices.size());
  for (unsigned int i = 0; i < indices.size(); ++i) {
    ins[i] = inputs[indices[i]];
  }

  return paraMods[names[0]].first->Evaluate(ins);
}

Eigen::MatrixXd GaussianSpecification::ApplyInverseCovariance(Eigen::MatrixXd const        & vec,
                                                              vector<Eigen::VectorXd> const& inputs)
{
      assert(vec.rows() == dim.sum());

  vector<unsigned int> indices = paraMods[names[1]].second;

  vector<Eigen::VectorXd> ins(indices.size());
  for (unsigned int i = 0; i < indices.size(); ++i) {
    ins[i] = inputs[indices[i]];
  }

  switch (mode) {
  case ScaledIdentity :
    {
      assert(paraMods[names[1]].first->outputSize == 1);
      const double scalarCov = paraMods[names[1]].first->Evaluate(ins) (0);

      return vec / scalarCov;
    }

  case DiagonalCovariance :
    {
      const Eigen::VectorXd covDiag = paraMods[names[1]].first->Evaluate(ins);

      return (1.0 / covDiag.array()).matrix().asDiagonal() * vec;
    }

  case DiagonalPrecision :
    {
      const Eigen::VectorXd precDiag = paraMods[names[1]].first->Evaluate(ins);

      return precDiag.asDiagonal() * vec;
    }

  case CovarianceMatrix :
    {
      if (paraMods[names[1]].first->inputSizes.size() > 0) {
        ComputeCholesky(ins);
      }

      Eigen::MatrixXd solved = cholDecomp.triangularView<Eigen::Lower>().solve(vec);
      cholDecomp.triangularView<Eigen::Lower>().transpose().solveInPlace(solved);

      return solved;
    }

  case PrecisionMatrix :
    {
      Eigen::VectorXd precVec = paraMods[names[1]].first->Evaluate(ins);
      const Eigen::MatrixXd prec(Eigen::Map<Eigen::MatrixXd>(precVec.data(), dim.sum(), dim.sum()));

      return prec * vec;
    }

  default:
  {
    assert((mode == ScaledIdentity) || (mode == DiagonalCovariance) ||
           (mode == PrecisionMatrix) || (mode == DiagonalPrecision));
    return Eigen::MatrixXd();
  }
  }
}

double GaussianSpecification::LogDeterminant(std::vector<Eigen::VectorXd> const& inputs)
{
  vector<unsigned int> indices = paraMods[names[1]].second;

  vector<Eigen::VectorXd> ins(indices.size());
  for (unsigned int i = 0; i < indices.size(); ++i) {
    ins[i] = inputs[indices[i]];
  }

  switch (mode) {
  case ScaledIdentity:
  {
    assert(paraMods[names[1]].first->outputSize == 1);
    const double scalarCov = paraMods[names[1]].first->Evaluate(ins) (0);

    return dim.sum() * log(scalarCov);
  }

  case DiagonalCovariance:
  {
    const Eigen::VectorXd covDiag = paraMods[names[1]].first->Evaluate(ins);

    return covDiag.array().log().sum();
  }

  case DiagonalPrecision:
  {
    const Eigen::VectorXd precDiag = paraMods[names[1]].first->Evaluate(ins);

    return -1.0 * (precDiag.array().log().sum());
  }

  case CovarianceMatrix:
  {
    if (paraMods[names[1]].first->inputSizes.size() > 0) {
      ComputeCholesky(ins);
    }

    return 2.0 * cholDecomp.diagonal().array().log().sum();
  }

  case PrecisionMatrix:
  {
    if (paraMods[names[1]].first->inputSizes.size() > 0) {
      ComputeCholesky(ins);
    }

    return -2.0 * cholDecomp.diagonal().array().log().sum();
  }

  default:
  {
    assert((mode == ScaledIdentity) || (mode == DiagonalCovariance) ||
           (mode == PrecisionMatrix) || (mode == DiagonalPrecision));
    return 0.0;
  }
  }
}

bool GaussianSpecification::HasInverseCovariance() const
{
  return mode != CovarianceMatrix;
}

Eigen::MatrixXd GaussianSpecification::GetPrecisionMatrix(vector<Eigen::VectorXd> const& inputs)
{
  vector<unsigned int> indices = paraMods[names[1]].second;

  vector<Eigen::VectorXd> ins(indices.size());
  for (unsigned int i = 0; i < indices.size(); ++i) {
    ins[i] = inputs[indices[i]];
  }

  switch (mode) {
  case ScaledIdentity:
  {
    assert(paraMods[names[1]].first->outputSize == 1);
    double scalarCov = paraMods[names[1]].first->Evaluate(ins) (0);

    return Eigen::MatrixXd::Identity(dim.sum(), dim.sum()) / scalarCov;
  }

  case DiagonalCovariance:
  {
    const Eigen::VectorXd covDiag = paraMods[names[1]].first->Evaluate(ins);

    return (1.0 / covDiag.array()).matrix().asDiagonal();
  }

  case DiagonalPrecision:
  {
    const Eigen::VectorXd precDiag = paraMods[names[1]].first->Evaluate(ins);

    return precDiag.asDiagonal();
  }

  case PrecisionMatrix:
  {
    Eigen::VectorXd precVec = paraMods[names[1]].first->Evaluate(ins);
    const Eigen::MatrixXd prec(Eigen::Map<Eigen::MatrixXd>(precVec.data(), dim.sum(), dim.sum()));

    return prec;
  }

  case CovarianceMatrix:
  {
	ComputeCholesky(ins);
    return CholSolver.solve(Eigen::MatrixXd::Identity(dim.sum(),dim.sum()));
  }
  
  default:
  {
  	std::cerr << "\nERROR: Invalid specification type.\n\n";
    assert(false);
	  
    return Eigen::MatrixXd();
  }
  }
}

Eigen::MatrixXd GaussianSpecification::GetCovarianceMatrix(vector<Eigen::VectorXd> const& inputs)
{
  vector<unsigned int> indices = paraMods[names[1]].second;

  vector<Eigen::VectorXd> ins(indices.size());
  for (unsigned int i = 0; i < indices.size(); ++i) {
    ins[i] = inputs[indices[i]];
  }

  switch (mode) {
  case ScaledIdentity:
  {
    assert(paraMods[names[1]].first->outputSize == 1);
    double scalarCov = paraMods[names[1]].first->Evaluate(ins) (0);

    return Eigen::MatrixXd::Identity(dim.sum(), dim.sum()) * scalarCov;
  }

  case DiagonalCovariance:
  {
    const Eigen::VectorXd covDiag = paraMods[names[1]].first->Evaluate(ins);

    return covDiag.asDiagonal();
  }

  case DiagonalPrecision:
  {
    const Eigen::VectorXd precDiag = paraMods[names[1]].first->Evaluate(ins);

    return precDiag.array().inverse().matrix().asDiagonal();
  }

  case PrecisionMatrix:
  {
    ComputeCholesky(ins);
    return CholSolver.solve(Eigen::MatrixXd::Identity(dim.sum(),dim.sum()));
  }

  case CovarianceMatrix:
  {
    Eigen::VectorXd covVec = paraMods[names[1]].first->Evaluate(ins);
    const Eigen::MatrixXd cov(Eigen::Map<Eigen::MatrixXd>(covVec.data(), dim.sum(), dim.sum()));

    return cov;
  }
  default:
  {
	std::cerr << "\nERROR: Invalid specification type.\n\n";
    assert(false);

    return Eigen::MatrixXd();
  }
  }
}

Eigen::MatrixXd GaussianSpecification::ApplyCovariance(Eigen::MatrixXd const& x, vector<Eigen::VectorXd> const& inputs)
{
  vector<unsigned int> indices = paraMods[names[1]].second;

  vector<Eigen::VectorXd> ins(indices.size());
  for (unsigned int i = 0; i < indices.size(); ++i) {
    ins[i] = inputs[indices[i]];
  }

  switch (mode) {
  case ScaledIdentity:
  {
    assert(paraMods[names[1]].first->outputSize == 1);
    double scalarCov = paraMods[names[1]].first->Evaluate(ins) (0);

    return scalarCov*x;
  }

  case DiagonalCovariance:
  {
    const Eigen::VectorXd covDiag = paraMods[names[1]].first->Evaluate(ins);

    return covDiag.asDiagonal()*x;
  }

  case DiagonalPrecision:
  {
    const Eigen::VectorXd precDiag = paraMods[names[1]].first->Evaluate(ins);

    return precDiag.array().inverse().matrix().asDiagonal()*x;
  }

  case PrecisionMatrix:
  {
    ComputeCholesky(ins);
    return CholSolver.solve(x);
  }

  case CovarianceMatrix:
  {
    Eigen::VectorXd covVec = paraMods[names[1]].first->Evaluate(ins);
    Eigen::Map<Eigen::MatrixXd> covMat(covVec.data(), dim.sum(), dim.sum());
 
    return covMat*x;
  }
  default:
  {
	std::cerr << "\nERROR: Invalid specification type.\n\n";
    assert(false);

    return Eigen::MatrixXd();
  }
  }
}

shared_ptr<ModGraph> GaussianSpecification::FullGraph(Eigen::VectorXd const  & mu,
                                                      Eigen::MatrixXd        & mat,
                                                      SpecificationMode const& mode)
{
  auto graph = make_shared<ModGraph>();

  // dimension
  const unsigned int N = mu.size();

  // add node for mean
  graph->AddNode(make_shared<ModParameter>(mu), "mean");

  // add node for covariance
  Eigen::VectorXd vec(Eigen::Map<Eigen::VectorXd>(mat.data(), N * N));
  graph->AddNode(make_shared<ModParameter>(vec), (mode == CovarianceMatrix) ? "cov" : "prec");

  return graph;
}

void GaussianSpecification::ComputeCholesky(vector<Eigen::VectorXd> const& inputs)
{
  if((inputs.size()!=0)||(!choleskyComputed)){
		
    Eigen::VectorXd vec = paraMods[names[1]].first->Evaluate(inputs);
    const Eigen::MatrixXd mat(Eigen::Map<Eigen::MatrixXd>(vec.data(), dim.sum(), dim.sum()));

    // we have the matrix, we may need the factorization
    CholSolver.compute(mat);
    cholDecomp = CholSolver.matrixL();
  
    choleskyComputed = true;
  }
  
}

shared_ptr<ModGraph> GaussianSpecification::ConditionalGraph(unsigned int const       M,
                                                             Eigen::VectorXd const  & mu,
                                                             Eigen::MatrixXd        & mat,
                                                             SpecificationMode const& mode)
{
  // full dimension
  const unsigned int N = mu.size();

    assert(N >= M);

  /// compute the covariance or precistion
  Eigen::MatrixXd vec;
  switch (mode) {
  case CovarianceMatrix: {
    auto graph = make_shared<ModGraph>();

    const Eigen::MatrixXd A = mat.topLeftCorner(M, M);
    const Eigen::MatrixXd B = mat.bottomRightCorner(N - M, N - M);
    const Eigen::MatrixXd C = mat.bottomLeftCorner(N - M, M);

    auto Binv = make_shared<LinearModel>(Eigen::VectorXd::Zero(N - M), B, true);

    graph->AddNode(make_shared<ModParameter>(-1.0 * mu.tail(N - M)), "conditional mean");
    graph->AddNode(make_shared<SumModel>(2, N - M), "conditional difference");
    graph->AddNode(Binv, "apply B inverse");
    graph->AddNode(make_shared<LinearModel>(mu.head(M), C.transpose()), "mean");

    graph->AddEdge("conditional mean", "conditional difference", 1);
    graph->AddEdge("conditional difference", "apply B inverse", 0);
    graph->AddEdge("apply B inverse", "mean", 0);

    const Eigen::MatrixXd L = Binv->GetCholesky();

    Eigen::MatrixXd solved = L.triangularView<Eigen::Lower>().solve(C);
    L.triangularView<Eigen::Lower>().transpose().solveInPlace(solved);

    Eigen::MatrixXd cov = A - C.transpose() * solved;

    // add node for covariance
    Eigen::VectorXd vec(Eigen::Map<Eigen::VectorXd>(cov.data(), M * M));
    graph->AddNode(make_shared<ModParameter>(vec), "cov");

    return graph;
  }

  case PrecisionMatrix:
  {
    auto graph = make_shared<ModGraph>();

    Eigen::MatrixXd A       = mat.topLeftCorner(M, M);
    const Eigen::MatrixXd C = mat.bottomLeftCorner(N - M, M);

    graph->AddNode(make_shared<ModParameter>(-1.0 * mu.tail(N - M)), "conditional mean");
    graph->AddNode(make_shared<SumModel>(2, N - M), "conditional difference");
    graph->AddNode(make_shared<LinearModel>(C.transpose()), "apply C transpose");
    graph->AddNode(make_shared<LinearModel>(mu.head(M), A, true), "mean");

    graph->AddEdge("conditional mean", "conditional difference", 1);
    graph->AddEdge("conditional difference", "apply C transpose", 0);
    graph->AddEdge("apply C transpose", "mean", 0);

    // add node for precision
    Eigen::VectorXd vec(Eigen::Map<Eigen::VectorXd>(A.data(), M * M));
    graph->AddNode(make_shared<ModParameter>(vec), "prec");

    return graph;
  }

  default:
  {
    assert((mode == CovarianceMatrix) || (mode == PrecisionMatrix));
    return nullptr;
  }
  }
}

Eigen::MatrixXd GaussianSpecification::ApplyCovarianceSqrt(Eigen::MatrixXd const        & mat,
                                                           vector<Eigen::VectorXd> const& inputs)
{
    assert(mat.rows() == dim.sum());

  vector<unsigned int> indices = paraMods[names[1]].second;

  vector<Eigen::VectorXd> ins(indices.size());
  for (unsigned int i = 0; i < indices.size(); ++i) {
    ins[i] = inputs[indices[i]];
  }

  switch (mode) {
  case ScaledIdentity:
  {
    assert(paraMods[names[1]].first->outputSize == 1);
    const double scalarCov = paraMods[names[1]].first->Evaluate(ins) (0);

    return sqrt(scalarCov) * mat;
  }

  case CovarianceMatrix:
  {
    if (paraMods[names[1]].first->inputSizes.size() > 0) {
      ComputeCholesky(ins);
    }

    return cholDecomp.triangularView<Eigen::Lower>() * mat;
  }

  case DiagonalCovariance:
  {
    const Eigen::VectorXd covDiag = paraMods[names[1]].first->Evaluate(ins);

    return covDiag.cwiseSqrt().asDiagonal() * mat;
  }

  case PrecisionMatrix:
  {
    if (paraMods[names[1]].first->inputSizes.size() > 0) {
      ComputeCholesky(ins);
    }

    return cholDecomp.triangularView<Eigen::Lower>().transpose().solve(mat);
  }

  case DiagonalPrecision:
  {
    const Eigen::VectorXd precDiag = paraMods[names[1]].first->Evaluate(ins);

    return (1.0 / precDiag.array()).matrix().cwiseSqrt().asDiagonal() * mat;
  }

  default:
  {
    assert((mode == ScaledIdentity) || (mode == DiagonalCovariance) ||
           (mode == PrecisionMatrix) || (mode == DiagonalPrecision));
    return Eigen::MatrixXd();
  }
  }
}


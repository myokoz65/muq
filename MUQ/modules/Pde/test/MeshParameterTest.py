import unittest

import libmuqModelling
import libmuqUtilities
import libmuqPde

import numpy as np

# Mesh parameter class without inputs
class NoInputMP(libmuqPde.MeshParameter):
    def __init__(self, system, param):
        super(NoInputMP, self).__init__(system, param)

    def ParameterEvaluate(self, x, y, z, var):
        return x

# Mesh parameter class with inputs
class InputMP(libmuqPde.MeshParameter):
    def __init__(self, system, param):
        super(InputMP, self).__init__(system, param)

    def ParameterEvaluate(self, x, y, z, var, inputs):
        return inputs[1][1] * (x + inputs[0] + inputs[1][0])

class MeshParameterTest(unittest.TestCase):
    def testNoInputs(self):
        param = dict()

        # options for the mesh
        param["mesh.type"] = "LineMesh"
        param["mesh.N"]    = 10
        param["mesh.L"]    = 1.0

        # create a system of equations
        system = libmuqPde.GenericEquationSystems(param)

        # options for the parameters 
        param["MeshParameter.SystemName"]     = "NoInputMP"
        param["MeshParameter.VariableNames"]  = "x,y"
        param["MeshParameter.VariableOrders"] = "1,2"

        mod = NoInputMP(system, param)

        expected = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,
                    0.0, 0.1, 0.05, 0.2, 0.15, 0.3, 0.25, 0.4, 0.35, 0.5, 0.45, 0.6, 0.55, 0.7, 0.65, 0.8, 0.75, 0.9, 0.85, 1.0, 0.95]

        result = mod.Evaluate()

        self.assertEqual(len(result), len(expected))
        for i in range(len(expected)):
            self.assertAlmostEqual(expected[i], result[i], 12)

    def testInputs(self):
        param = dict()

        # options for the mesh
        param["mesh.type"] = "LineMesh"
        param["mesh.N"]    = 10
        param["mesh.L"]    = 1.0

        # create a system of equations
        system = libmuqPde.GenericEquationSystems(param)

        # options for the parameters 
        param["MeshParameter.SystemName"]     = "InputMP"
        param["MeshParameter.VariableNames"]  = "x,y"
        param["MeshParameter.VariableOrders"] = "1,2"
        param["MeshParameter.InputNames"]     = "a,b"
        param["MeshParameter.InputSizes"]     = "1,2"

        mod = InputMP(system, param)

        expected = [2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0,
                    2.0, 2.1, 2.05, 2.2, 2.15, 2.3, 2.25, 2.4, 2.35, 2.5, 2.45, 2.6, 2.55, 2.7, 2.65, 2.8, 2.75, 2.9, 2.85, 3.0, 2.95]
        expected = (2.0 * np.array(expected)).tolist()

        result = mod.Evaluate([[1.0], [1.0, 2.0]])

        self.assertEqual(len(result), len(expected))
        for i in range(len(expected)):
            self.assertAlmostEqual(expected[i], result[i], 12)


#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Pde/MeshParameter.h"
#include "MUQ/Pde/PointObserver.h"

using namespace std;
using namespace muq::Pde;

/// Some random class to sense
class SomethingToObserve : public MeshParameter {
public:

  SomethingToObserve(string const                            & paraName,
                     shared_ptr<GenericEquationSystems> const& parasystem,
                     boost::property_tree::ptree const       & para) : 
    MeshParameter(paraName, parasystem, para) {}

private:

  /// Evaluate the mesh parameter
  virtual inline libMesh::Number ParameterEvaluate(double const x, double const y, double const,
                                                   string const& name) const override
  {
    if (name.compare("s1") == 0) {
      return 1.0 + x * y + 2.0 * x * x - 3.0 * y * y;
    }
    if (name.compare("s2") == 0) {
      return 2.0 - 2.0 * (1.0 - x) * y - 4.0 * x * x + 6.0 * y * y;
    }

    return 0.0;
  }
};

REGISTER_MESH_PARAMETER(SomethingToObserve)

TEST(Observer, PointObserverTest)
{
  boost::property_tree::ptree param;

  param.put("mesh.type", "SquareMesh");
  param.put("mesh.Nx", 25);
  param.put("mesh.Ny", 20);
  param.put("mesh.Lx", 1.0);
  param.put("mesh.Ly", 1.0);

  auto system = make_shared<GenericEquationSystems>(param);

  param.put("MeshParameter.SystemName", "SomethingToObserve");
  param.put("MeshParameter.VariableNames", "s1,s2");
  param.put("MeshParameter.VariableOrders", "2,1");

  // create a random field in the system that we need to sense
  auto toSense = MeshParameter::Create(system, param);

  // evaluate to get the field
  Eigen::VectorXd result = toSense->Evaluate();

  // choose some random points to put sensors on
  vector<Eigen::VectorXd> points(6);
  for (unsigned int i = 0; i < 6; ++i) {
    points[i].resize(2);
    points[i](0) = (double)i / 5.0;
    points[i](1) = (5.0 - (double)i) / 5.0;
  }

  // create the sensor
  auto sensor = make_shared<PointObserver>(toSense->outputSize * Eigen::VectorXi::Ones(1),
                                           points, 0, "SomethingToObserve", system);

  // evaluate the sensor on the field
  Eigen::VectorXd sensedPoints = sensor->Evaluate(result);

  for (unsigned int i = 0; i < 6; ++i) {
    const double x = points[i](0);
    const double y = points[i](1);

    EXPECT_DOUBLE_EQ(sensedPoints(2 * i),     1.0 + x * y + 2.0 * x * x - 3.0 * y * y);
    EXPECT_DOUBLE_EQ(sensedPoints(2 * i + 1), 2.0 - 2.0 * (1.0 - x) * y - 4.0 * x * x + 6.0 * y * y);
  }
}

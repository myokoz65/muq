import unittest
import sys

import numpy as np

import libmuqPde
import libmuqGeostatistics

class MeshKernelModelTest(unittest.TestCase):
    def setUp(self):
        self.param = dict()  
        
        self.param["mesh.type"] = "SquareMesh"
        self.param["mesh.Nx"]   = 13
        self.param["mesh.Ny"]   = 8
        self.param["mesh.Lx"]   = 1.0
        self.param["mesh.Ly"]   = 1.0

        self.param["MeshKernelModel.Name"] = "paraName"
        self.param["MeshKernelModel.KLModes"] = 100

        L   = 0.2 # length scale
        P   = 2.0 # kernel power 
        sig = 1.0 # variance

        self.kernel = libmuqGeostatistics.PowerKernel(L, P, sig)
        self.system = libmuqPde.GenericEquationSystems(self.param)

        self.numBasis = 100

    def testLinearShapeFunction(self):
        self.param["MeshKernelModel.Order"] = 1
        self.model = libmuqPde.MeshKernelModel(self.system, self.kernel, self.param)
        self.assertEqual(self.model.outputSize, 126)

    def testQuadraticShapeFunction(self):
        self.param["MeshKernelModel.Order"] = 2
        self.model = libmuqPde.MeshKernelModel(self.system, self.kernel, self.param)
        self.assertEqual(self.model.outputSize, 459)

    def tearDown(self):
        self.assertEqual(len(self.model.inputSizes), 1)
        self.assertEqual(self.model.inputSizes[0], self.numBasis)

        # destory the things to prevent memory leak
        del self.system 
        del self.model

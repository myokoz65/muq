#!/bin/bash
set +e

cd $WORKSPACE/MUQ/build
if [ -f CMakeCache.txt ]
then
  cmake ../
else
 
compiler=$1
echo "input compiler = " $compiler
case "$compiler" in 
gcc47)
        echo 'using gcc47'
    cmake \
    -DCMAKE_INSTALL_PREFIX=$WORKSPACE/install \
    -DCMAKE_CXX_COMPILER=g++-4.7 -DCMAKE_C_COMPILER=gcc-4.7 \
    -DMUQ_GTEST_DIR=/home/uqlab/gtests/gcc47/ \
    -DMUQ_USE_OPENMPI=OFF \
    -DMUQ_USE_GTEST=ON \
    ../
;;
gcc48)
    echo 'using gcc48'
    cmake \
      -DCMAKE_INSTALL_PREFIX=$WORKSPACE/install \
      -DCMAKE_CXX_COMPILER=g++-4.8 -DCMAKE_C_COMPILER=gcc-4.8 \
      -DCMAKE_BUILD_TYPE=Debug \
      -DCMAKE_CXX_FLAGS="-fprofile-arcs -ftest-coverage" \
      -DMUQ_GTEST_DIR=/home/uqlab/gtests/gcc48/ \
      -DMUQ_USE_OPENMPI=OFF \
      -DMUQ_USE_GTEST=ON \
      ../
;;
clang)
echo 'using clang'
    cmake \
      -DCMAKE_INSTALL_PREFIX=$WORKSPACE/install \
      -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang \
      -DMUQ_GTEST_DIR=/home/uqlab/gtests/clang/ \
      -DMUQ_USE_OPENMPI=OFF \
      -DMUQ_USE_GTEST=ON \
      ../
;;
esac
echo 'done'
fi
make

exit 0
